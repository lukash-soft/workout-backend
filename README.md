# WORKOUT BACKEND
[![pipeline status](https://gitlab.com/lukash-soft/workout-backend/badges/develop/pipeline.svg)](https://gitlab.com/lukash-soft/workout-backend/-/commits/develop)
# How to run

#Docker Commands
https://docs.docker.com/engine/reference/commandline/docker/

#Docker commands
#####1. If docker container is empty and you have no images uploaded:
 
**docker-compose pull** - pulls the images from the gitLab

**docker-compose up -d** - starts and runs a container in a detached mode

#####2. If you have images and containers running 

**docker-compose down** - you stop all containers

**docker rmi** - you remove all the images

**docker-compose pull** - you pull the images 

**docker-compose up -d** - you start the containers in a detached mode



