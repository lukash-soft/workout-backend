package com.lukash.workout.configuration;

public class ApplicationException extends RuntimeException {
    public ApplicationException(final String message) {
        super(message);
    }
}
