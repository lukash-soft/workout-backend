package com.lukash.workout.configuration;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {

    @GetMapping("ping")
    public String ping() {
        return "pong";
    }
}
