package com.lukash.workout.configuration.security.service;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserDatabaseDetailsService implements UserDetailsService {

    private final UserAccountRepository accountRepository;

    /**
     * This method is created so that the terminated date always
     * ends at 00:00 of the day after the termination date.
     * If we have terminated = 12.12.2021,
     * the account terminates at 00:00 of 13.12.2021
     **/
    @Override
    public UserDetails loadUserByUsername(final String email) {
        final UserAccount applicationUser =
                accountRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException(email));
        final List<GrantedAuthority> permissions =
                List.of(new SimpleGrantedAuthority(applicationUser.getAccountRole().value));

        final boolean isActive = Optional.ofNullable(applicationUser.getTerminatedDate())
                .map(terminatedDate -> !applicationUser.getTerminatedDate().isBefore(LocalDate.now()))
                .orElse(true);

        return new User(
                applicationUser.getEmail(),
                applicationUser.getPassword(),
                true,
                isActive,
                true,
                true,
                permissions);
    }
}
