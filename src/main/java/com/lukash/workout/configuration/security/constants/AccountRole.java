package com.lukash.workout.configuration.security.constants;

import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@RequiredArgsConstructor
public enum AccountRole {

    COACH(RoleValues.ROLE_COACH), CLIENT(RoleValues.ROLE_CLIENT), ADMIN(RoleValues.ROLE_ADMIN);

    public final String value;

    public static AccountRole fromValue(final String value) {
        return Arrays.stream(AccountRole.values())
                .filter(role -> role.value.equals(value))
                .findFirst().orElseThrow();
    }

    public interface RoleValues {
        String ROLE_CLIENT = "ROLE_CLIENT";
        String ROLE_COACH = "ROLE_COACH";
        String ROLE_ADMIN = "ROLE_ADMIN";
    }
}
