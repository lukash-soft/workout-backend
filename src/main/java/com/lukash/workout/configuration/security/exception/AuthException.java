package com.lukash.workout.configuration.security.exception;

import com.lukash.workout.configuration.ApplicationException;

public class AuthException extends ApplicationException {

    public AuthException(final String message) {
        super(message);
    }
}
