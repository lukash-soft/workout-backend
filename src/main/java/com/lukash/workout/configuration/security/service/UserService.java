package com.lukash.workout.configuration.security.service;

import com.lukash.workout.configuration.security.exception.AuthException;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserAccountRepository accountRepository;

    public UserAccount getLoggedAccount() {
        final String email = getUserPrincipal().getUsername();
        return accountRepository.findByEmail(email)
                .orElseThrow(() -> new AuthException("Could not find logged user"));
    }

    private User getUserPrincipal() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (User) authentication.getPrincipal();
    }
}
