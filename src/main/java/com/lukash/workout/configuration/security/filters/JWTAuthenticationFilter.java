package com.lukash.workout.configuration.security.filters;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lukash.workout.configuration.security.exception.AuthException;
import com.lukash.workout.feature.account.model.UserAccount;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;
import static com.lukash.workout.configuration.security.constants.SecurityConstants.*;

@RequiredArgsConstructor
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;

    @Override
    public Authentication attemptAuthentication(final HttpServletRequest req,
                                                final HttpServletResponse res) {
        final UserAccount account = parseAccountFromRequest(req);
        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(
                        account.getEmail(),
                        account.getPassword(),
                        new ArrayList<>());

        return authenticationManager.authenticate(token);
    }

    private UserAccount parseAccountFromRequest(final HttpServletRequest req) {
        try {
            return new ObjectMapper().readValue(req.getInputStream(), UserAccount.class);
        } catch (IOException e) {
            throw new AuthException("Unable to parse request");
        }
    }

    @Override
    protected void successfulAuthentication(final HttpServletRequest req,
                                            final HttpServletResponse res,
                                            final FilterChain chain,
                                            final Authentication auth) {
        final User user = (User) auth.getPrincipal();

        final String token = JWT.create()
                .withSubject(user.getUsername())
                .withClaim(ROLE_KEY, user.getAuthorities().iterator().next().getAuthority())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(HMAC512(SECRET.getBytes()));
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
    }
}
