package com.lukash.workout.feature.workoutPlan.repository;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.workoutPlan.model.WorkoutPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface WorkoutPlanRepository extends JpaRepository<WorkoutPlan, Long> {

    List<WorkoutPlan> findByCoach(UserAccount coach);

    List<WorkoutPlan> findByClientsContaining(UserAccount client);

    Optional<WorkoutPlan> findByCoachAndId(UserAccount coach, Long planId);

    Optional<WorkoutPlan> findByClientsContainingAndId(UserAccount client, Long planId);
}
