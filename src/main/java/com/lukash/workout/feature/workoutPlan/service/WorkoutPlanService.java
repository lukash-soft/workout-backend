package com.lukash.workout.feature.workoutPlan.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.workoutPlan.dto.WorkoutPlanDto;
import com.lukash.workout.feature.workoutPlan.exception.RoleNotFoundException;
import com.lukash.workout.feature.workoutPlan.model.Status;
import com.lukash.workout.feature.workoutPlan.model.WorkoutPlan;
import com.lukash.workout.feature.workoutPlan.repository.WorkoutPlanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WorkoutPlanService {

    private final UserService userService;
    private final WorkoutPlanRepository workoutPlanRepository;
    private final UserAccountRepository userAccountRepository;

    public List<WorkoutPlanDto> getWorkoutPlan() {
        final UserAccount loggedUser = userService.getLoggedAccount();

        switch (loggedUser.getAccountRole()) {
            case CLIENT:
                return WorkoutPlanDto.ofList(workoutPlanRepository.findByClientsContaining(loggedUser));
            case COACH:
                return WorkoutPlanDto.ofList(workoutPlanRepository.findByCoach(loggedUser));
            default:
                throw new RoleNotFoundException("Role not found");
        }
    }

    public WorkoutPlanDto saveWorkoutPlan(WorkoutPlanDto fromForm) {
        final UserAccount loggedUser = userService.getLoggedAccount();

        List<UserAccount> clients = userAccountRepository.findAllById(fromForm.getClientIds());

        WorkoutPlan workoutPlanToSave = WorkoutPlan.builder()
                .clients(clients)
                .status(fromForm.getStatus())
                .workouts(fromForm.getWorkouts())
                .coach(loggedUser)
                .build();

        workoutPlanRepository.save(workoutPlanToSave);
        return WorkoutPlanDto.of(workoutPlanToSave);
    }


    public WorkoutPlanDto updateWorkoutPlan(WorkoutPlanDto fromForm) {
        final UserAccount loggedUser = userService.getLoggedAccount();
        final WorkoutPlan existingWorkoutPlan = workoutPlanRepository.findByCoachAndId(loggedUser, fromForm.getId())
                .orElseThrow();

        List<UserAccount> clients = userAccountRepository.findAllById(fromForm.getClientIds());

        existingWorkoutPlan.setStatus(fromForm.getStatus());
        existingWorkoutPlan.setWorkouts(fromForm.getWorkouts());
        existingWorkoutPlan.setClients(clients);

        workoutPlanRepository.save(existingWorkoutPlan);
        return WorkoutPlanDto.of(existingWorkoutPlan);
    }

    public void removeWorkoutPlan(Long id) {
        final UserAccount loggedUser = userService.getLoggedAccount();

        WorkoutPlan planToRemove = workoutPlanRepository.findByCoachAndId(loggedUser, id).orElseThrow();

        workoutPlanRepository.delete(planToRemove);
    }

    public WorkoutPlanDto updateStatus(Long planId, Status status) {
        UserAccount loggedUser = userService.getLoggedAccount();

        return WorkoutPlanDto.of(getWorkoutPlanDto(planId, status, loggedUser));
    }

    public WorkoutPlan getWorkoutPlanDto(Long planId, Status status, UserAccount loggedUser) {
        if (loggedUser.getAccountRole().equals(AccountRole.COACH)) {
            WorkoutPlan workoutPlan = workoutPlanRepository.findByCoachAndId(loggedUser, planId).orElseThrow();
            workoutPlan.setStatus(status);
            return workoutPlan;
        } else {
            WorkoutPlan workoutPlan = workoutPlanRepository.findByClientsContainingAndId(loggedUser, planId).orElseThrow();
            workoutPlan.setStatus(status);
            return workoutPlan;
        }
    }
}

