package com.lukash.workout.feature.workoutPlan.model;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.workoutPlan.dto.WorkoutDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkoutPlan {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(optional = false)
    private UserAccount coach;
    @OneToMany
    @Builder.Default
    private List<UserAccount> clients = new ArrayList<>();
    @Column(nullable = false)
    @Builder.Default
    private Status status = Status.READY;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @Builder.Default
    private List<WorkoutDto> workouts = new ArrayList<>();
    @Column(nullable = false, updatable = false)
    @Builder.Default
    private LocalDateTime dateCreated = LocalDateTime.now();
}
