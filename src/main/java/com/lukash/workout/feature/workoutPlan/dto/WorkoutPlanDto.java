package com.lukash.workout.feature.workoutPlan.dto;

import com.lukash.workout.feature.account.dto.UserAccountDto;
import com.lukash.workout.feature.workoutPlan.model.Status;
import com.lukash.workout.feature.workoutPlan.model.WorkoutPlan;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class WorkoutPlanDto {

    private Long id;
    private UserAccountDto coach;
    private List<UserAccountDto> clients;
    private Status status;
    private List<WorkoutDto> workouts;
    private LocalDateTime dateCreated;

    public static WorkoutPlanDto of(final WorkoutPlan workoutPlan) {
        return new WorkoutPlanDto(workoutPlan.getId(),
                UserAccountDto.of(workoutPlan.getCoach()),
                UserAccountDto.ofList(workoutPlan.getClients()),
                workoutPlan.getStatus(),
                workoutPlan.getWorkouts(),
                workoutPlan.getDateCreated());
    }

    public static List<WorkoutPlanDto> ofList(final List<WorkoutPlan> workoutPlansList) {
        List<WorkoutPlanDto> workoutPlansDto = new ArrayList<>();

        for (WorkoutPlan workoutPlan : workoutPlansList) {
            workoutPlansDto.add(WorkoutPlanDto.of(workoutPlan));
        }
        return workoutPlansDto;
    }

    public List<Long> getClientIds() {
        List<Long> clientsId = new ArrayList<>();

        for (UserAccountDto userAccount : clients) {
            clientsId.add(userAccount.getId());
        }
        return clientsId;
    }
}
