package com.lukash.workout.feature.workoutPlan.controller;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.workoutPlan.dto.WorkoutPlanDto;
import com.lukash.workout.feature.workoutPlan.model.Status;
import com.lukash.workout.feature.workoutPlan.service.WorkoutPlanService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("workout")
@RequiredArgsConstructor
public class WorkoutPlanController {

    private final WorkoutPlanService workoutPlanService;

    @GetMapping
    public List<WorkoutPlanDto> get() {
        return workoutPlanService.getWorkoutPlan();
    }

    @Secured(AccountRole.RoleValues.ROLE_COACH)
    @PostMapping
    public WorkoutPlanDto create(@RequestBody WorkoutPlanDto workoutPlanDto) {
        return workoutPlanService.saveWorkoutPlan(workoutPlanDto);
    }

    @Secured(AccountRole.RoleValues.ROLE_COACH)
    @PutMapping
    public WorkoutPlanDto update(@RequestBody WorkoutPlanDto workoutPlanFromForm) {
        return workoutPlanService.updateWorkoutPlan(workoutPlanFromForm);
    }

    @PutMapping("status/{id}/{status}")
    public WorkoutPlanDto updateStatus(@PathVariable Long id, @PathVariable Status status) {
        return workoutPlanService.updateStatus(id, status);
    }

    @Secured(AccountRole.RoleValues.ROLE_COACH)
    @DeleteMapping("{workoutPlanId}")
    public void delete(@PathVariable Long workoutPlanId) {
        workoutPlanService.removeWorkoutPlan(workoutPlanId);
    }
}
