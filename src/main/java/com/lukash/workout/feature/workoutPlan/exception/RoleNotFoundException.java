package com.lukash.workout.feature.workoutPlan.exception;

import com.lukash.workout.configuration.security.constants.AccountRole;

public class RoleNotFoundException extends RuntimeException {

    public RoleNotFoundException(String message) {
        super(message);
    }

    public RoleNotFoundException(AccountRole accountRole){
        super("Unrecognized role: " + accountRole);
    }
}