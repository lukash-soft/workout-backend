package com.lukash.workout.feature.workoutPlan.model;

public enum Status {
    READY, COMPLETED
}
