package com.lukash.workout.feature.athlete_details.controller;

import com.lukash.workout.feature.athlete_details.model.AthleteDetails;
import com.lukash.workout.feature.athlete_details.service.AthleteDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("athleteDetails")
@RequiredArgsConstructor
public class AthleteDetailsController {

    private final AthleteDetailsService athleteDetailsService;

    @PutMapping
    public AthleteDetails updateAthleteDetails(@RequestBody AthleteDetails athleteDetails) {
        return athleteDetailsService.updateAthleteDetails(athleteDetails);
    }

    @GetMapping
    public List<AthleteDetails> getAthleteDetailsForUser() {
        return athleteDetailsService.getAthleteDetailsForUser();
    }

    @PostMapping
    public AthleteDetails addAthleteDetails(@RequestBody AthleteDetails athleteDetails) {
        return athleteDetailsService.addAthleteDetails(athleteDetails);
    }

    @DeleteMapping("{athleteDetailsId}")
    public void deleteAthleteDetails(@PathVariable Long athleteDetailsId) {
        athleteDetailsService.deleteAthleteDetails(athleteDetailsId);
    }

}
