package com.lukash.workout.feature.athlete_details.model;

import com.lukash.workout.feature.account.model.UserAccount;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.OffsetDateTime;

@Entity
@Builder
@NoArgsConstructor
@Data
@AllArgsConstructor
public class AthleteDetails {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    @Builder.Default
    private OffsetDateTime added = OffsetDateTime.now();

    @Column
    @Min(value = 0)
    @Schema(example = "120")
    @Builder.Default
    private double weight = 0;

    @Column
    @Min(value = 0)
    @Schema(example = "178")
    @Builder.Default
    private double height = 0;

    @Column
    @Schema(example = "123")
    @Min(value = 0)
    @Builder.Default
    private Double waistMeasurement = 0.0;

    @Column
    @Schema(example = "123")
    @Min(value = 0)
    @Builder.Default
    private Double thighMeasurement = 0.0;

    @Column
    @Schema(example = "123")
    @Min(value = 0)
    @Builder.Default
    private Double armMeasurement = 0.0;

    @Column
    @Min(value = 0)
    @Schema(example = "120")
    @Builder.Default
    private double benchPress = 0;

    @Column
    @Min(value = 0)
    @Schema(example = "79")
    @Builder.Default
    private double squat = 0;

    @ManyToOne(optional = false)
    private UserAccount userAccount;
}
