package com.lukash.workout.feature.athlete_details.repository;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.athlete_details.model.AthleteDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AthleteDetailsRepository extends JpaRepository<AthleteDetails, Long> {

    Optional<AthleteDetails> findByUserAccountId(Long userId);
    Optional<AthleteDetails> findByUserAccountIdAndId(Long userId, Long detailsId);
    List<AthleteDetails> findByUserAccount(UserAccount loggedUser);

}
