package com.lukash.workout.feature.athlete_details.service;

import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.athlete_details.model.AthleteDetails;
import com.lukash.workout.feature.athlete_details.repository.AthleteDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AthleteDetailsService {

    private final AthleteDetailsRepository athleteDetailsRepository;

    private final UserService userService;

    public AthleteDetails addAthleteDetails(AthleteDetails fromForm) {
        UserAccount loggedUser = userService.getLoggedAccount();

        AthleteDetails athleteDetails = AthleteDetails.builder()
                .userAccount(loggedUser)
                .height(fromForm.getHeight())
                .weight(fromForm.getWeight())
                .benchPress(fromForm.getBenchPress())
                .squat(fromForm.getSquat())
                .armMeasurement(fromForm.getArmMeasurement())
                .thighMeasurement(fromForm.getThighMeasurement())
                .waistMeasurement(fromForm.getWaistMeasurement())
                .build();

        return athleteDetailsRepository.save(athleteDetails);
    }

    public AthleteDetails updateAthleteDetails(AthleteDetails fromForm) {
        final UserAccount loggedUser = userService.getLoggedAccount();
        final AthleteDetails existingAthleteDetails =
                athleteDetailsRepository.findByUserAccountIdAndId(loggedUser.getId(), fromForm.getId()).orElseThrow();

        existingAthleteDetails.setSquat(fromForm.getSquat());
        existingAthleteDetails.setBenchPress(fromForm.getBenchPress());
        existingAthleteDetails.setHeight(fromForm.getHeight());
        existingAthleteDetails.setWeight(fromForm.getWeight());
        existingAthleteDetails.setWaistMeasurement(fromForm.getWaistMeasurement());
        existingAthleteDetails.setArmMeasurement(fromForm.getArmMeasurement());
        existingAthleteDetails.setThighMeasurement(fromForm.getThighMeasurement());

        return athleteDetailsRepository.save(existingAthleteDetails);
    }

    public List<AthleteDetails> getAthleteDetailsForUser() {
        UserAccount loggedUser = userService.getLoggedAccount();
        return athleteDetailsRepository.findByUserAccount(loggedUser);
    }

    public void deleteAthleteDetails(Long athleteDetailsId) {
        final UserAccount loggedUser = userService.getLoggedAccount();

        AthleteDetails detailsToRemove = athleteDetailsRepository.findByUserAccountIdAndId(loggedUser.getId(),athleteDetailsId).orElseThrow();

        athleteDetailsRepository.delete(detailsToRemove);
    }
}
