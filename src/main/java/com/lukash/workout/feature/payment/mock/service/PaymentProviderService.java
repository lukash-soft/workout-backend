package com.lukash.workout.feature.payment.mock.service;

import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.payment.dto.PaymentDto;
import com.lukash.workout.feature.payment.invoicing.model.Invoice;
import com.lukash.workout.feature.payment.invoicing.repository.InvoiceRepository;
import com.lukash.workout.feature.payment.mock.exception.CardException;
import com.lukash.workout.feature.payment.mock.repository.PaymentProviderRepository;
import com.lukash.workout.feature.payment.model.Payment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class PaymentProviderService {

    private final static int cardLength = 16;
    private final static int cvcLength = 3;
    private final PaymentProviderRepository paymentProviderRepository;
    private final UserService userService;
    private final InvoiceRepository invoiceRepository;

    @Transactional
    public void savePayment(PaymentDto paymentDto) {
        final UserAccount loggedUser = userService.getLoggedAccount();
        String cardNumber = paymentDto.getCardNumber();

        validateCardDetails(paymentDto, cardNumber);

        final Invoice invoice = invoiceRepository.findByIdAndUser(paymentDto.getInvoiceId(), loggedUser).orElseThrow();

        String lastFourDigits = cardNumber.trim()
                .substring(paymentDto.getCardNumber().length() - 4);

        Payment payment = Payment.builder()
                .id(paymentDto.getId())
                .amount(paymentDto.getAmountPaid())
                .cardNumber(lastFourDigits)
                .user(loggedUser)
                .build();

        Payment paymentDone = paymentProviderRepository.save(payment);
        invoice.setPayment(paymentDone);
        invoiceRepository.save(invoice);
    }

    private void validateCardDetails(PaymentDto paymentDto, String cardNumber) {
        if (cardNumber.length() != cardLength
                || paymentDto.getSecurityCode().length() != cvcLength
                || paymentDto.getExpirationDate().isBefore(LocalDate.now())) {
            throw new CardException("Invalid card details");
        }
    }
}
