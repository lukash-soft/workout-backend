package com.lukash.workout.feature.payment.mock.repository;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.payment.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PaymentProviderRepository extends JpaRepository<Payment, Long> {

    Optional<Payment> findByUser(UserAccount loggedUser);
}
