package com.lukash.workout.feature.payment.invoicing.repository;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.payment.invoicing.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    List<Invoice> findByCreatedDayAfterAndUser(LocalDate createdDay, UserAccount user);
    Optional<Invoice> findByIdAndUser(Long invoiceId, UserAccount user);
    List<Invoice> findByCreatedDayEqualsOrCreatedDayAfter(LocalDate createdDay, LocalDate createdDayAfter);

}
