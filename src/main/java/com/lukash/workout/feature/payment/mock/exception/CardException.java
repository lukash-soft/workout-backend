package com.lukash.workout.feature.payment.mock.exception;

public class CardException extends RuntimeException {

    public CardException(String message) {
        super(message);
    }
}
