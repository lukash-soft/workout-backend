package com.lukash.workout.feature.payment.invoicing.service;

import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.payment.invoicing.dto.InvoiceDto;
import com.lukash.workout.feature.payment.invoicing.model.Invoice;
import com.lukash.workout.feature.payment.invoicing.repository.InvoiceRepository;
import com.lukash.workout.feature.subscription.model.Subscription;
import com.lukash.workout.feature.subscription.repository.SubscriptionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Log4j2
public class InvoiceService {

    private final InvoiceRepository invoiceRepository;
    private final UserService userService;
    private final SubscriptionRepository subscriptionRepository;

    public List<InvoiceDto> getForUser(LocalDate from) {
        final UserAccount loggedUser = userService.getLoggedAccount();

        List<Invoice> invoices = invoiceRepository.findByCreatedDayAfterAndUser(from, loggedUser);

        return InvoiceDto.ofList(invoices);
    }

    @Scheduled(cron = "0 0 0 1 * *")
    @Transactional
    void create() {
        log.info("Scheduled job for invoices is starting");
        List<Subscription> activeSubscriptions = subscriptionRepository.findByEndDateNullOrEndDateAfter(LocalDate.now());
        Map<UserAccount, List<Subscription>> userToSubscriptionMap = prepareUserAndSubscriptionsMap(activeSubscriptions);

        LocalDate firstDayOfTheMonth = LocalDate.now().withDayOfMonth(1);

        List<Invoice> invoicesForTimePeriod = invoiceRepository.findByCreatedDayEqualsOrCreatedDayAfter(firstDayOfTheMonth, firstDayOfTheMonth);

        if (!invoicesForTimePeriod.isEmpty()) {
            log.info("Quitting invoiceCreate method because of existing invoices.");
            return;
        }
        for (Map.Entry<UserAccount, List<Subscription>> entry : userToSubscriptionMap.entrySet()) {
            double totalAmountToPay = calculateTotalAmountToPay(entry);

            Invoice invoice = Invoice.builder()
                    .user(entry.getKey())
                    .totalAmount(totalAmountToPay)
                    .build();

            invoiceRepository.save(invoice);
        }
    }

    private double calculateTotalAmountToPay(Map.Entry<UserAccount, List<Subscription>> entry) {
        double totalAmountToPay = 0.0;
        List<Subscription> list = entry.getValue();
        for (int i = 0; i < list.size(); i++) {
            totalAmountToPay += list.get(i).getFee();
        }
        return totalAmountToPay;
    }

    private Map<UserAccount, List<Subscription>> prepareUserAndSubscriptionsMap(List<Subscription> activeSubscriptions) {
        Map<UserAccount, List<Subscription>> userToSubscriptionMap = new HashMap<>();

        for (Subscription subscription : activeSubscriptions) {
            if (userToSubscriptionMap.containsKey(subscription.getClient())) {
                userToSubscriptionMap.get(subscription.getClient()).add(subscription);
            } else {
                List<Subscription> usersSubscriptions = new ArrayList<>();
                usersSubscriptions.add(subscription);
                userToSubscriptionMap.put(subscription.getClient(), usersSubscriptions);
            }
        }

        return userToSubscriptionMap;
    }

}
