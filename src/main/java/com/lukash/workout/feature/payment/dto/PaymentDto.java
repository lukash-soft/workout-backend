package com.lukash.workout.feature.payment.dto;

import com.lukash.workout.feature.payment.model.Payment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaymentDto {

    private Long id;
    private String cardNumber;
    private LocalDate expirationDate;
    private String securityCode;
    private Double amountPaid;
    private Long invoiceId;

    public static PaymentDto of(Payment payment) {
        return PaymentDto.builder()
                .id(payment.getId())
                .securityCode(payment.getCardNumber())
                .cardNumber(payment.getCardNumber())
                .amountPaid(payment.getAmount())
                .build();
    }
}
