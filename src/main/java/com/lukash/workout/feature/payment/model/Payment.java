package com.lukash.workout.feature.payment.model;

import com.lukash.workout.feature.account.model.UserAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Payment {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    @Builder.Default
    private LocalDate paymentDate= LocalDate.now();
    @Column(nullable = false)
    @Min(0)
    private Double amount;
    @Column(nullable = false)
    private String cardNumber;
    @OneToOne(optional = false)
    private UserAccount user;
}
