package com.lukash.workout.feature.payment.invoicing.model;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.payment.model.Payment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    @Builder.Default
    private LocalDate createdDay = LocalDate.now();
    @OneToOne(optional = false)
    private UserAccount user;
    @Column(nullable = false)
    private Double totalAmount;
    @OneToOne
    private Payment payment;
}
