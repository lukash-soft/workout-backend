package com.lukash.workout.feature.payment.invoicing.dto;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.payment.invoicing.model.Invoice;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceDto {

    private Long id;
    private LocalDate createdDay;
    private UserAccount user;
    private Double totalAmount;
    private Boolean isPaid;


    public static InvoiceDto of(final Invoice invoice) {

        boolean isPaid = invoice.getPayment() != null;

        return new InvoiceDto(
                invoice.getId(),
                invoice.getCreatedDay(),
                invoice.getUser(),
                invoice.getTotalAmount(),
                isPaid
        );
    }

    public static List<InvoiceDto> ofList(final List<Invoice> invoiceList) {
        List<InvoiceDto> invoiceDtos = new ArrayList<>();

        for (Invoice invoice : invoiceList) {
            invoiceDtos.add(InvoiceDto.of(invoice));
        }
        return invoiceDtos;
    }
}
