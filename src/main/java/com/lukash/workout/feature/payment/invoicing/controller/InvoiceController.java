package com.lukash.workout.feature.payment.invoicing.controller;

import com.lukash.workout.feature.payment.invoicing.dto.InvoiceDto;
import com.lukash.workout.feature.payment.invoicing.service.InvoiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/invoice")
@RequiredArgsConstructor
public class InvoiceController {

    private final InvoiceService invoiceService;

    @GetMapping
    public List<InvoiceDto> getForUser(@RequestParam(required = false) LocalDate from) {
        final LocalDate defaultDate = Optional.ofNullable(from)
                .orElse(LocalDate.now()
                        .minusMonths(6)
                        .with(TemporalAdjusters.firstDayOfMonth())
                );

        return invoiceService.getForUser(defaultDate);
    }
}
