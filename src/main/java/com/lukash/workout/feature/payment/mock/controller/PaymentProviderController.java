package com.lukash.workout.feature.payment.mock.controller;

import com.lukash.workout.feature.payment.dto.PaymentDto;
import com.lukash.workout.feature.payment.mock.service.PaymentProviderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * This class serves as a mock provider through which we are going to process payments.
 * E.g. TPay etc.
 **/


@RestController
@RequestMapping("/paymentProvider")
@RequiredArgsConstructor
public class PaymentProviderController {

    private final PaymentProviderService paymentProviderService;

    @PostMapping
    public void savePayment(@RequestBody PaymentDto paymentDto) {
        paymentProviderService.savePayment(paymentDto);
    }
}
