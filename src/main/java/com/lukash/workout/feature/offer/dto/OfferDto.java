package com.lukash.workout.feature.offer.dto;

import com.lukash.workout.feature.offer.model.OfferModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OfferDto {

    private Long id;
    private String title;
    private String description;
    private Double price;
    private OfferUserDto coach;

    public static OfferDto of(final OfferModel offerModel) {
        return new OfferDto(
                offerModel.getId(),
                offerModel.getTitle(),
                offerModel.getDescription(),
                offerModel.getPrice(),
                OfferUserDto.of(offerModel.getCoach())
        );
    }

    public static List<OfferDto> ofList(final List<OfferModel> offers) {
        List<OfferDto> offersDto = new ArrayList<>();

        for (OfferModel offer : offers) {
            offersDto.add(OfferDto.of(offer));
        }

        return offersDto;
    }

}
