package com.lukash.workout.feature.offer.dto;

import com.lukash.workout.feature.account.model.UserAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OfferUserDto {

    private Long id;
    private String firstName;
    private String lastName;

    public static OfferUserDto of(final UserAccount userAccount) {

        return new OfferUserDto(
                userAccount.getId(),
                userAccount.getFirstName(),
                userAccount.getLastName()
        );
    }

}
