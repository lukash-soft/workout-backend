package com.lukash.workout.feature.offer.repository;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.offer.model.OfferModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OfferRepository extends JpaRepository<OfferModel, Long> {

    List<OfferModel> findByCoach(UserAccount coach);

    Optional<OfferModel> findByCoachAndId(UserAccount coach, Long offerId);

    List<OfferModel> findByIdNotIn(List<Long> offerIds);
}
