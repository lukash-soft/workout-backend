package com.lukash.workout.feature.offer.model;

import com.lukash.workout.feature.account.model.UserAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;

@Builder
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class OfferModel {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private String description;
    @Column(nullable = false)
    @Min(0)
    private Double price;
    @ManyToOne(optional = false)
    private UserAccount coach;

}
