package com.lukash.workout.feature.offer.service;

import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.offer.dto.OfferDto;
import com.lukash.workout.feature.offer.exception.PriceException;
import com.lukash.workout.feature.offer.model.OfferModel;
import com.lukash.workout.feature.offer.repository.OfferRepository;
import com.lukash.workout.feature.subscription.dto.SubscriptionDto;
import com.lukash.workout.feature.subscription.service.SubscriptionService;
import com.lukash.workout.feature.workoutPlan.exception.RoleNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OfferService {

    private final UserService userService;
    private final OfferRepository offerRepository;
    private final SubscriptionService subscriptionService;

    public List<OfferDto> getOffers() {
        final UserAccount loggedUser = userService.getLoggedAccount();

        switch (loggedUser.getAccountRole()) {
            case COACH:
                return OfferDto.ofList(offerRepository.findByCoach(loggedUser));
            case CLIENT:
                List<OfferModel> allOffers = offerRepository.findAll();
                List<SubscriptionDto> subscriptions = subscriptionService.getAllSubscriptionsForUser();
                List<Long> offerIds = subscriptions.stream()
                        .map(subscriptionDto -> subscriptionDto.getOfferId())
                        .collect(Collectors.toList());

                List<OfferModel> offersStillNotBought = allOffers.stream()
                        .filter(offerModel -> !offerIds.contains(offerModel.getId()))
                        .collect(Collectors.toList());

                return OfferDto.ofList(offersStillNotBought);
            default:
                throw new RoleNotFoundException(loggedUser.getAccountRole());
        }
    }

    @Transactional
    public OfferDto create(OfferDto fromForm) {
        if (fromForm.getPrice() <= 0) {
            throw new PriceException("Price is negative");
        }

        final UserAccount loggedUser = userService.getLoggedAccount();

        OfferModel offerToCreate = OfferModel.builder()
                .title(fromForm.getTitle())
                .description(fromForm.getDescription())
                .coach(loggedUser)
                .price(fromForm.getPrice())
                .build();

        return OfferDto.of(offerRepository.save(offerToCreate));
    }

    public OfferDto update(OfferDto fromForm) {
        final OfferModel existingOffer = offerRepository.findById(fromForm.getId()).orElseThrow();

        existingOffer.setTitle(fromForm.getTitle());
        existingOffer.setDescription(fromForm.getDescription());
        existingOffer.setPrice(fromForm.getPrice());

        return OfferDto.of(offerRepository.save(existingOffer));
    }

    public void delete(Long offerId) {
        final UserAccount loggedUser = userService.getLoggedAccount();
        final OfferModel offerToRemove = offerRepository.findByCoachAndId(loggedUser, offerId).orElseThrow();

        offerRepository.delete(offerToRemove);
    }
}
