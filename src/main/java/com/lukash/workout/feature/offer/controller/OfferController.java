package com.lukash.workout.feature.offer.controller;

import com.lukash.workout.feature.offer.dto.OfferDto;
import com.lukash.workout.feature.offer.service.OfferService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.lukash.workout.configuration.security.constants.AccountRole.RoleValues.ROLE_COACH;

@RestController
@RequestMapping("/offer")
@RequiredArgsConstructor
public class OfferController {

    private final OfferService offerService;

    @GetMapping
    public List<OfferDto> getOffers() {
        return offerService.getOffers();
    }

    @Secured(ROLE_COACH)
    @PostMapping
    public OfferDto create(@RequestBody OfferDto offerToCreate) {
        return offerService.create(offerToCreate);
    }

    @Secured(ROLE_COACH)
    @PutMapping
    public OfferDto update(@RequestBody OfferDto fromForm) {
        return offerService.update(fromForm);
    }

    @Secured(ROLE_COACH)
    @DeleteMapping("{offerId}")
    public void delete(@PathVariable Long offerId) {
        offerService.delete(offerId);
    }

}
