package com.lukash.workout.feature.account.controller;


import com.lukash.workout.feature.account.dto.RegistrationDto;
import com.lukash.workout.feature.account.service.UserAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.lukash.workout.configuration.security.constants.SecurityConstants.SIGN_UP_URL;

@RestController
@RequestMapping(SIGN_UP_URL)
@RequiredArgsConstructor
public class RegistrationController {

    private final UserAccountService accountService;

    @PostMapping
    public void registerAccount(@RequestBody final RegistrationDto registrationDto) {
        accountService.registerAccount(registrationDto);
    }
}
