package com.lukash.workout.feature.account.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RegistrationDto {

    private String firstName;
    private String lastName;
    private String email;
    private OffsetDateTime birthDate;
    private String password;
    private String role;

}
