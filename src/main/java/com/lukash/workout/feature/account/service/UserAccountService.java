package com.lukash.workout.feature.account.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.dto.RegistrationDto;
import com.lukash.workout.feature.account.dto.UserAccountDto;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.athlete_details.model.AthleteDetails;
import com.lukash.workout.feature.athlete_details.repository.AthleteDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserAccountService {

    private final PasswordEncoder encoder;
    private final UserAccountRepository userAccountRepository;
    private final AthleteDetailsRepository athleteDetailsRepository;
    private final UserService userService;
    private final AccountRemovalService accountRemovalService;

    public List<UserAccountDto> getAccounts() {
        return UserAccountDto.ofList(userAccountRepository.findAll());
    }

    public void removeAccount() {
        UserAccount userAccount = userService.getLoggedAccount();
        accountRemovalService.removeAccount(userAccount);
    }

    public UserAccountDto updateAccount(UserAccountDto dtoFromForm) {
        UserAccount userAccount = userService.getLoggedAccount();

        userAccount.setFirstName(dtoFromForm.getFirstName());
        userAccount.setLastName(dtoFromForm.getLastName());
        userAccount.setBirthDate(dtoFromForm.getBirthDate());
        userAccount.setBio(dtoFromForm.getBio());

        return UserAccountDto.of(userAccountRepository.save(userAccount));
    }


    public void updateBio(UserAccountDto userAccountDto) {
        UserAccount userAccount = userService.getLoggedAccount();

        userAccount.setBio(userAccountDto.getBio());

        userAccountRepository.save(userAccount);
    }


    public void registerAccount(final RegistrationDto registrationDto) {
        final UserAccount accountToRegister = UserAccount.builder()
                .firstName(registrationDto.getFirstName())
                .lastName(registrationDto.getLastName())
                .email(registrationDto.getEmail())
                .birthDate(registrationDto.getBirthDate())
                .password(encoder.encode(registrationDto.getPassword()))
                .accountRole(AccountRole.fromValue(registrationDto.getRole()))
                .build();

        final AthleteDetails athleteDetails = AthleteDetails.builder()
                .userAccount(accountToRegister)
                .build();

        if (accountToRegister.getBirthDate().isAfter(OffsetDateTime.now())) {
            throw new RuntimeException("Please correct the date.");
        }

        userAccountRepository.save(accountToRegister);
        athleteDetailsRepository.save(athleteDetails);
    }

    public UserAccountDto getUserDetails() {
        return UserAccountDto.of(userService.getLoggedAccount());
    }

    @Transactional
    public List<UserAccountDto> getAllNonConnectedUsers() {
        UserAccount loggedAccount = userService.getLoggedAccount();
        Set<Long> ids = loggedAccount.getFriends().stream()
                .map(friend -> friend.getId())
                .collect(Collectors.toSet());
        ids.add(loggedAccount.getId());

        return UserAccountDto.ofList(userAccountRepository.findByIdNotIn(ids));
    }

    @Transactional
    public List<UserAccountDto> getAllFriends() {
        UserAccount loggedUser = userService.getLoggedAccount();
        return UserAccountDto.ofList(loggedUser.getFriends());
    }

    public List<UserAccountDto> getClients() {
        return UserAccountDto.ofList(userAccountRepository.findByAccountRole(AccountRole.CLIENT));
    }
}
