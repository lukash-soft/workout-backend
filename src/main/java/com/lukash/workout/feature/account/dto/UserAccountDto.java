package com.lukash.workout.feature.account.dto;

import com.lukash.workout.feature.account.model.UserAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAccountDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private OffsetDateTime birthDate;
    private String bio;

    public static UserAccountDto of(final UserAccount userAccount) {
        return new UserAccountDto(
                userAccount.getId(),
                userAccount.getFirstName(),
                userAccount.getLastName(),
                userAccount.getEmail(),
                userAccount.getBirthDate(),
                userAccount.getBio());
    }

    public static List<UserAccountDto> ofList(final List<UserAccount> usersList) {
        List<UserAccountDto> dtoList = new ArrayList<>();

        for (UserAccount userAccount : usersList) {
            dtoList.add(UserAccountDto.of(userAccount));
        }
        return dtoList;
    }

}
