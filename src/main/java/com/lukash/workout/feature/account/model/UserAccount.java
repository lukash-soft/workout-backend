package com.lukash.workout.feature.account.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.lukash.workout.configuration.security.constants.AccountRole;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString(exclude = {"friends", "password"})
public class UserAccount {

    @Id
    @GeneratedValue
    private Long id;

    @Column(updatable = false, nullable = false)
    @Builder.Default
    private LocalDate created = LocalDate.now();

    private LocalDate terminatedDate;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AccountRole accountRole;

    @Column(nullable = false)
    @NotBlank(message = "The name cannot be null")
    @Schema(example = "John")
    private String firstName;

    @Column(nullable = false)
    @NotBlank(message = "The lastname cannot be null")
    @Schema(example = "Done")
    private String lastName;

    @Column(nullable = false)
    @NotBlank(message = "Please fill the date")
    @Past(message = "The date has to be in the past")
    @JsonFormat(pattern = "dd-MM-YYYY")
    @Schema(example = "1900-01-28")
    private OffsetDateTime birthDate;

    @Column(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)//we don't want to expose this to the world!
    @NotBlank(message = "Please fill the password")
    @Size(min = 3)
    private String password;

    @Column(unique = true, nullable = false)
    @Email
    @NotNull
    private String email;

    @Column
    private String bio;

    @JsonIgnore
    @ManyToMany
    @Builder.Default
    private List<UserAccount> friends = new ArrayList<>();

}
