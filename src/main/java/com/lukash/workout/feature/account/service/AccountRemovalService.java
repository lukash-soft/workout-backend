package com.lukash.workout.feature.account.service;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.athlete_details.repository.AthleteDetailsRepository;
import com.lukash.workout.feature.group.model.WorkoutGroup;
import com.lukash.workout.feature.group.repository.GroupRepository;
import com.lukash.workout.feature.invitation.model.Invitation;
import com.lukash.workout.feature.invitation.repository.InvitationRepository;
import com.lukash.workout.feature.message.model.Message;
import com.lukash.workout.feature.message.repository.MessageRepository;
import com.lukash.workout.feature.picutre.model.Picture;
import com.lukash.workout.feature.picutre.repository.PictureRepository;
import com.lukash.workout.feature.post.model.PostModel;
import com.lukash.workout.feature.post.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
class AccountRemovalService {

    private final GroupRepository groupRepository;
    private final InvitationRepository invitationRepository;
    private final PostRepository postRepository;
    private final PictureRepository pictureRepository;
    private final MessageRepository messageRepository;
    private final AthleteDetailsRepository athleteDetailsRepository;
    private final UserAccountRepository userAccountRepository;

    @Transactional
    void removeAccount(UserAccount userAccount) {
        athleteDetailsRepository.findByUserAccountId(userAccount.getId()).ifPresent(
                athleteDetails -> athleteDetailsRepository.delete(athleteDetails));
        List<WorkoutGroup> userAccountGroups = groupRepository.findByAdmin_Id(userAccount.getId());
        List<Invitation> userAccountInvitationsFrom = invitationRepository.findAllByAccountFrom_Id(userAccount.getId());
        List<Invitation> userAccountInvitationsTo = invitationRepository.findAllByAccountTo_Id(userAccount.getId());
        List<PostModel> userAccountPosts = postRepository.findAllByCreatorId(userAccount.getId());
        List<Picture> userAccountPictures = pictureRepository.getByAddedBy_Id(userAccount.getId());
        List<Message> userAccountMessagesFrom = messageRepository.findAllBySenderId(userAccount.getId());
        List<UserAccount> friends = userAccount.getFriends();

        for (UserAccount userAccountFriend : friends) {
            userAccountFriend.getFriends().remove(userAccount);
        }

        groupRepository.deleteAll(userAccountGroups);
        invitationRepository.deleteAll(userAccountInvitationsFrom);
        invitationRepository.deleteAll(userAccountInvitationsTo);
        postRepository.deleteAll(userAccountPosts);
        pictureRepository.deleteAll(userAccountPictures);
        messageRepository.deleteAll(userAccountMessagesFrom);
        userAccountRepository.delete(userAccount);
    }
}
