package com.lukash.workout.feature.account.controller;

import com.lukash.workout.feature.account.dto.UserAccountDto;
import com.lukash.workout.feature.account.service.UserAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.lukash.workout.configuration.security.constants.AccountRole.RoleValues.ROLE_ADMIN;

@RestController
@RequestMapping("accounts")
@RequiredArgsConstructor
public class UserAccountController {

    private final UserAccountService userAccountService;

    @GetMapping("test")
    public String test() {
        return "test";
    }

    @GetMapping("nonConnectedUsers")
    public List<UserAccountDto> getAllNonConnectedUsers() {
        return userAccountService.getAllNonConnectedUsers();
    }

    @GetMapping("friends")
    public List<UserAccountDto> getFriends() {
        return userAccountService.getAllFriends();
    }

    @GetMapping("clients")
    public List<UserAccountDto> getClients(){
        return userAccountService.getClients();
    }

    @Secured({ROLE_ADMIN})
    @GetMapping
    public List<UserAccountDto> getAllAccounts() {
        return userAccountService.getAccounts();
    }

    @GetMapping("details")
    public UserAccountDto getAccountDetails() {
        return userAccountService.getUserDetails();
    }

    @PutMapping
    public UserAccountDto updateAccount(@RequestBody UserAccountDto userAccountDto) {
        return userAccountService.updateAccount(userAccountDto);
    }

    @PutMapping("/bio")
    public void updateBio(@RequestBody UserAccountDto userAccountDto) {
        userAccountService.updateBio(userAccountDto);
    }

    @DeleteMapping
    public void removeAccount() {
        userAccountService.removeAccount();
    }
}

