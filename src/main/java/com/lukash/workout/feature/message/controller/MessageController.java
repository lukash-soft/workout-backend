package com.lukash.workout.feature.message.controller;

import com.lukash.workout.feature.message.model.Message;
import com.lukash.workout.feature.message.service.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("messsages")
@RequiredArgsConstructor
public class MessageController {

    private final MessageService messageService;

    @GetMapping("{userId}")
    public List<Message> getUserMesssage(@PathVariable Long userId) {
        return messageService.getAllForUser(userId);
    }

    @PostMapping
    public Message saveMessage(@RequestBody Message message) {
        return messageService.saveMessage(message);
    }

    @DeleteMapping("{messageId}")
    public void deleteMessage(@PathVariable Long messageId) {
        messageService.removeMessage(messageId);
    }
}
