package com.lukash.workout.feature.message.repository;

import com.lukash.workout.feature.message.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message,Long> {
    List<Message> findAllBySenderId(Long userFromId);
}
