package com.lukash.workout.feature.message.model;
import com.lukash.workout.feature.account.model.UserAccount;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Message {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @Schema (example = "2020-06-20")
    private LocalDateTime created;

    @ManyToOne(optional = false)
    private UserAccount sender;

    @ManyToOne(optional = false)
    private UserAccount recipient;

    @Column(nullable = false)
    @NotBlank
    @Schema(example = "This is content of the message")
    private String content;


}
