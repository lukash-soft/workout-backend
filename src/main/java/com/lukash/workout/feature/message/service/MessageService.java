package com.lukash.workout.feature.message.service;
import com.lukash.workout.feature.message.model.Message;
import com.lukash.workout.feature.message.repository.MessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MessageService {


    private final MessageRepository messageRepository;

    public List<Message> getAllForUser(final Long userId) {
        return messageRepository.findAll();
    }

    public Message saveMessage (Message message) {
       return messageRepository.save(message);
    }

    public void removeMessage (Long messageId) {
        messageRepository.deleteById(messageId);
    }

}
