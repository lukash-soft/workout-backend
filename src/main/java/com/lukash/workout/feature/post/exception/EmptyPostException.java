package com.lukash.workout.feature.post.exception;

import com.lukash.workout.configuration.ApplicationException;

public class EmptyPostException extends ApplicationException {

    public EmptyPostException(String message) {
        super(message);
    }
}
