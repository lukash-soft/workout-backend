package com.lukash.workout.feature.post.model;

import com.lukash.workout.feature.account.model.UserAccount;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PostModel {

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    @NotBlank
    @Schema(example = "Test title")
    private String title;
    @Column(nullable = false)
    @Schema(example = "Content to test")
    private String content;
    @Column
    @Schema(example = "2020-11-20")
    private LocalDateTime created;
    @ManyToOne
    private UserAccount creator;
}
