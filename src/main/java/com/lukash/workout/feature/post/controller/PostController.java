package com.lukash.workout.feature.post.controller;

import com.lukash.workout.feature.post.model.PostModel;
import com.lukash.workout.feature.post.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("posts")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;

    @GetMapping
    public List<PostModel> shouldGetAll() {
        return postService.getAll();
    }

    @GetMapping("user")
    public List<PostModel> shouldGetAllForUser() {
        return postService.getAllForUser();
    }

    @PostMapping
    public PostModel savePost(@RequestBody PostModel postToSave) {
        return postService.savePost(postToSave);
    }

    @PutMapping()
    public PostModel updatePost(@RequestBody PostModel updatePost) {
        return postService.updatePost(updatePost);
    }

    @DeleteMapping("{postId}")
    public void removePost(@PathVariable Long postId) {
        postService.removePost(postId);
    }

}
