package com.lukash.workout.feature.post.service;

import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.post.exception.EmptyPostException;
import com.lukash.workout.feature.post.model.PostModel;
import com.lukash.workout.feature.post.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;

    private final UserService userService;

    public List<PostModel> getAll() {
        return postRepository.findAll();
    }

    public List<PostModel> getAllForUser() {
        UserAccount loggedUser = userService.getLoggedAccount();
        return postRepository.findAllByCreatorId(loggedUser.getId());
    }

    public PostModel updatePost(PostModel postFromForm) {
        UserAccount loggedUser = userService.getLoggedAccount();
        PostModel existingPost = postRepository.findByCreator_IdAndId(loggedUser.getId(), postFromForm.getId()).orElseThrow();

        existingPost.setContent(postFromForm.getContent());
        existingPost.setTitle(postFromForm.getTitle());


        return postRepository.save(existingPost);
    }

    public PostModel savePost(PostModel postToAdd) {
        if (postToAdd.getContent().isEmpty()) {
            throw new EmptyPostException("Content is empty. Please fill.");
        }

        UserAccount loggedUser = userService.getLoggedAccount();
        postToAdd.setCreator(loggedUser);
        postToAdd.setCreated(LocalDateTime.now());
        postToAdd.setContent(postToAdd.getContent());

        return postRepository.save(postToAdd);
    }

    public void removePost(Long postId) {
        UserAccount loggedUser = userService.getLoggedAccount();
        PostModel postToRemove = postRepository.findByCreator_IdAndId(loggedUser.getId(), postId).orElseThrow();

        postRepository.delete(postToRemove);
    }
}
