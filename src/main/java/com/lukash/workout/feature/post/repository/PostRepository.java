package com.lukash.workout.feature.post.repository;

import com.lukash.workout.feature.post.model.PostModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PostRepository extends JpaRepository<PostModel, Long> {

    List<PostModel> findAllByCreatorId (Long userId);

    Optional<PostModel> findByCreator_IdAndId (Long userId, Long postId);
}
