package com.lukash.workout.feature.subscription.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NewSubscriptionFormDto {

    private Long clientId;
    private Double fee;

}
