package com.lukash.workout.feature.subscription.controller;

import com.lukash.workout.feature.offer.dto.OfferDto;
import com.lukash.workout.feature.subscription.dto.SubscriptionDto;
import com.lukash.workout.feature.subscription.service.SubscriptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.lukash.workout.configuration.security.constants.AccountRole.RoleValues.ROLE_CLIENT;
import static com.lukash.workout.configuration.security.constants.AccountRole.RoleValues.ROLE_COACH;

@RestController
@RequestMapping("subscription")
@RequiredArgsConstructor
public class SubscriptionController {

    private final SubscriptionService subscriptionService;

    @GetMapping
    public List<SubscriptionDto> getSubscriptionsForUser() {
        return subscriptionService.getAllSubscriptionsForUser();
    }

    @Secured(ROLE_CLIENT)
    @PostMapping
    public SubscriptionDto createSubscription(@RequestBody OfferDto fromForm) {
        return subscriptionService.createSubscription(fromForm);
    }

    @Secured(ROLE_COACH)
    @PutMapping("fee")
    public SubscriptionDto updateSubscriptionFee(@RequestBody SubscriptionDto fromForm) {
        return subscriptionService.updateSubscriptionFee(fromForm);
    }

    @Secured(ROLE_CLIENT)
    @PutMapping("/cancel/{planId}")
    public SubscriptionDto cancelSubscription(@PathVariable Long planId) {
        return subscriptionService.cancelSubscription(planId);
    }

    @Secured(ROLE_COACH)
    @DeleteMapping("{subscriptionId}")
    public void removeSubscription(@PathVariable Long subscriptionId) {
        subscriptionService.removeSubscription(subscriptionId);
    }

}
