package com.lukash.workout.feature.subscription.dto;

import com.lukash.workout.feature.account.dto.UserAccountDto;
import com.lukash.workout.feature.subscription.model.Subscription;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubscriptionDto {

    private Long id;
    private UserAccountDto coach;
    private UserAccountDto client;
    private LocalDate created;
    private LocalDate ended;
    private Double fee;
    private Long offerId;

    public static SubscriptionDto of(final Subscription subscription) {
        return new SubscriptionDto(
                subscription.getId(),
                UserAccountDto.of(subscription.getCoach()),
                UserAccountDto.of(subscription.getClient()),
                subscription.getCreated(),
                subscription.getEndDate(),
                subscription.getFee(),
                subscription.getOfferId()
        );
    }

    public static List<SubscriptionDto> ofList(final List<Subscription> subscriptionList) {
        List<SubscriptionDto> subscriptionDtos = new ArrayList<>();

        for (Subscription subscription : subscriptionList) {
            subscriptionDtos.add(SubscriptionDto.of(subscription));
        }

        return subscriptionDtos;
    }
}
