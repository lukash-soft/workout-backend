package com.lukash.workout.feature.subscription.model;

import com.lukash.workout.feature.account.model.UserAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.LocalDate;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Subscription {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private UserAccount coach;
    @ManyToOne(optional = false)
    private UserAccount client;
    @Column(updatable = false, nullable = false)
    @Builder.Default
    private LocalDate created = LocalDate.now();
    @Column
    private LocalDate endDate;
    @Column
    private Long offerId;
    @Column(nullable = false)
    @Min(0)
    @Builder.Default
    private Double fee = 0.0;

}
