package com.lukash.workout.feature.subscription.repository;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.subscription.model.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {

    Optional<Subscription> findByCoachAndId(UserAccount coach, Long subscriptionId);

    List<Subscription> findByCoach(UserAccount coach);

    List<Subscription> findByClient(UserAccount client);

    Optional<Subscription> findByClientAndId(UserAccount client, Long subId);

    List<Subscription> findByEndDateNullOrEndDateAfter(LocalDate date);
}
