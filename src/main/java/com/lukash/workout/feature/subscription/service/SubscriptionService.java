package com.lukash.workout.feature.subscription.service;

import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.offer.dto.OfferDto;
import com.lukash.workout.feature.subscription.dto.SubscriptionDto;
import com.lukash.workout.feature.subscription.model.Subscription;
import com.lukash.workout.feature.subscription.repository.SubscriptionRepository;
import com.lukash.workout.feature.workoutPlan.exception.RoleNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SubscriptionService {

    private final UserService userService;
    private final SubscriptionRepository subscriptionRepository;
    private final UserAccountRepository userAccountRepository;


    public List<SubscriptionDto> getAllSubscriptionsForUser() {
        return SubscriptionDto.ofList(getByUser());
    }

    private List<Subscription> getByUser() {
        final UserAccount loggedUser = userService.getLoggedAccount();

        switch (loggedUser.getAccountRole()) {
            case COACH:
                return subscriptionRepository.findByCoach(loggedUser);
            case CLIENT:
                return subscriptionRepository.findByClient(loggedUser);
            default:
                throw new RoleNotFoundException(loggedUser.getAccountRole());
        }
    }

    public SubscriptionDto createSubscription(final OfferDto fromForm) {
        final UserAccount client = userService.getLoggedAccount();
        final UserAccount coach = userAccountRepository.findById(fromForm.getCoach().getId()).orElseThrow();

        Subscription subscriptionToCreate = Subscription.builder()
                .coach(coach)
                .client(client)
                .fee(fromForm.getPrice())
                .offerId(fromForm.getId())
                .build();

        return SubscriptionDto.of(subscriptionRepository.save(subscriptionToCreate));
    }

    public SubscriptionDto updateSubscriptionFee(SubscriptionDto fromForm) {
        final UserAccount loggedUser = userService.getLoggedAccount();
        final Subscription existingSubscription =
                subscriptionRepository.findByCoachAndId(loggedUser, fromForm.getId()).orElseThrow();

        existingSubscription.setFee(fromForm.getFee());
        subscriptionRepository.save(existingSubscription);

        return SubscriptionDto.of(existingSubscription);
    }

    public SubscriptionDto cancelSubscription(Long planId) {
        final Subscription existingSubscription =
                subscriptionRepository.findById(planId).orElseThrow();

        existingSubscription.setEndDate(LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()));
        subscriptionRepository.save(existingSubscription);

        return SubscriptionDto.of(existingSubscription);
    }

    public void removeSubscription(Long subscriptionId) {
        final UserAccount loggedUser = userService.getLoggedAccount();
        final Subscription subscriptionToDelete =
                subscriptionRepository.findByCoachAndId(loggedUser, subscriptionId).orElseThrow();

        subscriptionRepository.delete(subscriptionToDelete);
    }
}
