package com.lukash.workout.feature.invitation.model;

public enum InvitationStatus {
    PENDING, ACCEPTED, DECLINED
}
