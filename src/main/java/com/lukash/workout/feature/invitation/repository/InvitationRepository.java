package com.lukash.workout.feature.invitation.repository;


import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.invitation.model.Invitation;
import com.lukash.workout.feature.invitation.model.InvitationStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface InvitationRepository extends JpaRepository<Invitation, Long> {

    Optional<Invitation> findByAccountFrom(UserAccount userAccount);

    Optional<Invitation> findByAccountFrom_IdAndAccountTo_Id(Long invitingUserId, Long invitedUserId);

    Optional<Invitation> findByIdAndAccountFrom_Id(Long invitationId, Long accountFromId);

    List<Invitation> findAllByAccountFrom_Id(Long accountFromId);
    List<Invitation> findAllByAccountTo_Id(Long accountToId);

    Optional<Invitation> findByIdAndAccountTo_Id (Long invitationId, Long accountToId);

    List<Invitation> findAllByAccountToIdAndInvitationStatus(Long accountToId, InvitationStatus statusInv);
}
