package com.lukash.workout.feature.invitation.dto;

import com.lukash.workout.feature.invitation.model.Invitation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class InvitationDto {
    private List<Invitation> sent;
    private List<Invitation> received;

}
