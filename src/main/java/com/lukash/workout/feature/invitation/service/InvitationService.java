package com.lukash.workout.feature.invitation.service;


import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.invitation.dto.InvitationDto;
import com.lukash.workout.feature.invitation.model.Invitation;
import com.lukash.workout.feature.invitation.model.InvitationStatus;
import com.lukash.workout.feature.invitation.repository.InvitationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InvitationService {

    private final InvitationRepository invitationRepository;

    private final UserService userService;

    private final UserAccountRepository userAccountRepository;


    public InvitationDto getPendingInvitations() {
        UserAccount loggedUser = userService.getLoggedAccount();

        List<Invitation> sentInvitationsList = invitationRepository.findAllByAccountFrom_Id(loggedUser.getId());
        List<Invitation> receivedInvitationsList =
                invitationRepository.findAllByAccountToIdAndInvitationStatus(loggedUser.getId(), InvitationStatus.PENDING);

        return InvitationDto.builder()
                .received(receivedInvitationsList)
                .sent(sentInvitationsList)
                .build();
    }

    public Invitation declineInvitation(Long invitationId) {
        UserAccount invitationRecipient = userService.getLoggedAccount();
        Invitation invitationToDecline = invitationRepository.findByIdAndAccountTo_Id(invitationId, invitationRecipient.getId()).orElseThrow();

        invitationToDecline.setInvitationStatus(InvitationStatus.DECLINED);
        invitationToDecline.setEdited(LocalDateTime.now());

        return invitationRepository.save(invitationToDecline);
    }

    @Transactional
    public Invitation acceptInvitation(Long invitationId) {
        UserAccount recipientAccount = userService.getLoggedAccount();

        Invitation invitationToAccept = invitationRepository.findByIdAndAccountTo_Id(invitationId, recipientAccount.getId()).orElseThrow();
        invitationToAccept.setInvitationStatus(InvitationStatus.ACCEPTED);
        UserAccount invitationSender = userAccountRepository.findById(invitationToAccept.getAccountFrom().getId()).orElseThrow();

        recipientAccount.getFriends().add(invitationSender);
        invitationSender.getFriends().add(recipientAccount);

        return invitationRepository.save(invitationToAccept);
    }

    public Invitation inviteUser(Long invitedUserId) {
        UserAccount invitingUser = userService.getLoggedAccount();
        UserAccount invitedUser = userAccountRepository.findById(invitedUserId).orElseThrow();

        checkIfAlreadyExists(invitedUserId, invitingUser);

        Invitation invitation = Invitation.builder()
                .accountFrom(invitingUser)
                .accountTo(invitedUser)
                .created(LocalDateTime.now())
                .edited(LocalDateTime.now())
                .build();


        return invitationRepository.save(invitation);
    }

    public void checkIfAlreadyExists(Long invitedUserId, UserAccount invitingUser) {
        Optional<Invitation> optionalA = invitationRepository.findByAccountFrom_IdAndAccountTo_Id(invitingUser.getId(), invitedUserId);
        Optional<Invitation> optionalB = invitationRepository.findByAccountFrom_IdAndAccountTo_Id(invitedUserId, invitingUser.getId());

        if (optionalA.isPresent() || optionalB.isPresent()) {
            throw new RuntimeException("Duplciated invitation");
        }
    }

    public void cancelInvitation(Long invitationId) {
        UserAccount loggedUser = userService.getLoggedAccount();
        Invitation invitationToCancel = invitationRepository.findByIdAndAccountFrom_Id(invitationId, loggedUser.getId()).orElseThrow();

        invitationRepository.delete(invitationToCancel);
    }

    @Transactional
    public void friendToRemove(Long friendId) {
        UserAccount loggedUser = userService.getLoggedAccount();
        UserAccount friendToRemove = userAccountRepository.findById(friendId).orElseThrow();

        loggedUser.getFriends().remove(friendToRemove);
        friendToRemove.getFriends().remove(loggedUser);

        userAccountRepository.save(loggedUser);
        userAccountRepository.save(friendToRemove);
    }
}
