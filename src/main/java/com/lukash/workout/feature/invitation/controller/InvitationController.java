package com.lukash.workout.feature.invitation.controller;

import com.lukash.workout.feature.invitation.dto.InvitationDto;
import com.lukash.workout.feature.invitation.model.Invitation;
import com.lukash.workout.feature.invitation.service.InvitationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("invitations")
@RequiredArgsConstructor
public class InvitationController {


    private final InvitationService invitationService;

    @GetMapping
    public InvitationDto getPendingInvitations () {
        return invitationService.getPendingInvitations();
    }

    @PostMapping("{userId}")
    public Invitation inviteUser(@PathVariable Long userId) {
        return invitationService.inviteUser(userId);
    }

    @DeleteMapping("/cancel/{invitationId}")
    public void cancelInvitation(@PathVariable  Long invitationId) {
        invitationService.cancelInvitation(invitationId);
    }

    @PutMapping("/decline/{invitationIdDecline}")
    public Invitation declineInvitation(@PathVariable Long invitationIdDecline) {
        return invitationService.declineInvitation(invitationIdDecline);
    }

    @PutMapping("/accept/{invitationIdAccept}")
    public Invitation acceptInvitation(@PathVariable Long invitationIdAccept) {
        return invitationService.acceptInvitation(invitationIdAccept);
    }

    @DeleteMapping("/friend/{friendId}")
    public void removeFriend(@PathVariable Long friendId) {
        invitationService.friendToRemove(friendId);
    }

}
