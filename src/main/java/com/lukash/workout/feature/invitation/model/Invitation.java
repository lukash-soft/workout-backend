package com.lukash.workout.feature.invitation.model;

import com.lukash.workout.feature.account.model.UserAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;


@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Invitation {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(optional = false)
    private UserAccount accountFrom;
    @ManyToOne(optional = false)
    private UserAccount accountTo;
    @Column(nullable = false, updatable = false)
    @Builder.Default
    private LocalDateTime created = LocalDateTime.now();
    @Column(nullable = false)
    @Builder.Default
    private LocalDateTime edited = LocalDateTime.now();
    @Column(nullable = false)
    @Builder.Default
    private InvitationStatus invitationStatus = InvitationStatus.PENDING;
}
