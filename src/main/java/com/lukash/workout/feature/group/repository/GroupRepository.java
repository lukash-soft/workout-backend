package com.lukash.workout.feature.group.repository;

import com.lukash.workout.feature.group.model.WorkoutGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface GroupRepository extends JpaRepository<WorkoutGroup, Long> {

    List<WorkoutGroup> findByAdmin_Id (Long loggedUserId);

    Optional<WorkoutGroup> findByAdmin_IdAndId(Long adminId,Long groupId);

}
