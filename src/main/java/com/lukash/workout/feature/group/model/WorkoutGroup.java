package com.lukash.workout.feature.group.model;

import com.lukash.workout.feature.account.model.UserAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class WorkoutGroup {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @NotBlank
    private String groupName;

    @Column(nullable = false)
    @NotBlank
    private String groupType;

    @ManyToOne (optional = false)
    private UserAccount admin;

    @ManyToMany
    @Builder.Default
    private List<UserAccount> members = new ArrayList<>();

    @Override
    public String toString() {
        return "WorkoutGroup " +
                "id=" + id;
    }
}
