package com.lukash.workout.feature.group.service;

import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.group.model.WorkoutGroup;
import com.lukash.workout.feature.group.repository.GroupRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
@AllArgsConstructor
public class GroupService {

    private GroupRepository groupRepository;

    private UserAccountRepository userAccountRepository;

    private UserService userService;

    public List<WorkoutGroup> getAll() {
        return groupRepository.findAll();
    }

    public List<WorkoutGroup> getAllForLoggedUser() {
        UserAccount loggedUser = userService.getLoggedAccount();
        return groupRepository.findByAdmin_Id(loggedUser.getId());
    }

    public void addMember(final Long userId, final Long groupId) {
        UserAccount loggedUser = userService.getLoggedAccount();
        final WorkoutGroup group = groupRepository.findByAdmin_IdAndId(loggedUser.getId(), groupId).
                orElseThrow(() -> new RuntimeException("Group not found"));
        final UserAccount memberToAdd = userAccountRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException("User not found!!!"));

        if (group.getMembers().contains(memberToAdd)) {
            throw new RuntimeException("Member is already in the group.");
        }

        group.getMembers().add(memberToAdd);
        groupRepository.save(group);
    }

    public void removeMember(final Long userId, final Long groupId) {
        final UserAccount loggedUser = userService.getLoggedAccount();
        final WorkoutGroup group = groupRepository.findByAdmin_IdAndId(loggedUser.getId(), groupId)
                .orElseThrow(() -> new RuntimeException("Group not found!!!"));

        for (int i = 0; i < group.getMembers().size(); i++) {
            if (group.getMembers().get(i).getId().equals(userId)) {
                group.getMembers().remove(i);
                break;
            }
        }

        groupRepository.save(group);
    }


    public WorkoutGroup updateGroup(WorkoutGroup workoutGroupFromForm) {
        UserAccount loggedUser = userService.getLoggedAccount();
        WorkoutGroup existingGroup = groupRepository.findByAdmin_IdAndId(loggedUser.getId(), workoutGroupFromForm.getId()).orElseThrow();

        existingGroup.setGroupName(workoutGroupFromForm.getGroupName());
        existingGroup.setGroupType(workoutGroupFromForm.getGroupType());

        return groupRepository.save(existingGroup);
    }

    public void removeGroup(Long groupId) {
        UserAccount loggedUser = userService.getLoggedAccount();
        WorkoutGroup groupToRemove = groupRepository.findByAdmin_IdAndId(loggedUser.getId(), groupId).orElseThrow();

        groupRepository.delete(groupToRemove);
    }

    public WorkoutGroup saveGroup(WorkoutGroup fromForm) {
        UserAccount loggedUser = userService.getLoggedAccount();
        fromForm.setAdmin(loggedUser);

        return groupRepository.save(fromForm);
    }

    @Transactional
    public List<UserAccount> getMembersForGroup(Long groupId) {
        UserAccount loggedUser = userService.getLoggedAccount();
        WorkoutGroup workoutGroup = groupRepository.findByAdmin_IdAndId(loggedUser.getId(), groupId).orElseThrow();

        return workoutGroup.getMembers();
    }
}
