package com.lukash.workout.feature.group.controller;


import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.group.model.WorkoutGroup;
import com.lukash.workout.feature.group.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.lukash.workout.configuration.security.constants.AccountRole.RoleValues.ROLE_COACH;

@RestController
@RequestMapping("group")
@RequiredArgsConstructor
public class GroupController {

    private final GroupService groupService;

    @GetMapping
    public List<WorkoutGroup> getAll() {
        return groupService.getAll();
    }

    @GetMapping("user")
    public List<WorkoutGroup> getAllForLoggedUser() {
        return groupService.getAllForLoggedUser();
    }

    @PostMapping
    @Secured(ROLE_COACH)
    public WorkoutGroup saveGroup(@RequestBody WorkoutGroup workoutGroup) {
        return groupService.saveGroup(workoutGroup);
    }

    @PutMapping
    @Secured(ROLE_COACH)
    public WorkoutGroup updateGroup(@RequestBody WorkoutGroup workoutGroup) {
        return groupService.updateGroup(workoutGroup);
    }


    @GetMapping("{groupId}")
    public List<UserAccount> getMembersForGroup(@PathVariable Long groupId) {
        return groupService.getMembersForGroup(groupId);
    }

    @PutMapping("add/member/{memberId}/group/{groupId}")
    @Secured(ROLE_COACH)
    public void addMember(@PathVariable Long memberId, @PathVariable Long groupId) {
        groupService.addMember(memberId, groupId);
    }

    @PutMapping("remove/member/{memberId}/group/{groupId}")
    @Secured(ROLE_COACH)
    public void removeMember(@PathVariable Long memberId, @PathVariable Long groupId) {
        groupService.removeMember(memberId, groupId);
    }

    @DeleteMapping("{groupId}")
    @Secured(ROLE_COACH)
    public void removeGroup(@PathVariable Long groupId) {
        groupService.removeGroup(groupId);
    }
}
