package com.lukash.workout.feature.picutre.exception;

public class MissingFileException extends RuntimeException {
    public MissingFileException(Throwable cause) {
        super(cause);
    }

    public MissingFileException(String message) {
        super(message);
    }
}
