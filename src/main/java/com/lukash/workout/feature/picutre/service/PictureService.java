package com.lukash.workout.feature.picutre.service;

import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.picutre.exception.MissingFileException;
import com.lukash.workout.feature.picutre.exception.PictureException;
import com.lukash.workout.feature.picutre.model.Picture;
import com.lukash.workout.feature.picutre.repository.PictureRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class PictureService {

    static final String IMAGE_FOLDER_DIR = System.getProperty("user.dir") + "/IMAGES";

    private final PictureRepository pictureRepository;

    private final UserService userService;

    @PostConstruct //this method is run once after initialisation of this bean
    void posInit() {
        File directory = new File(IMAGE_FOLDER_DIR);
        createDirectoryIfNotExists(directory);
    }

    private void createDirectoryIfNotExists(File directory) {
        if (!directory.exists()) {
            try {
                Files.createDirectories(Paths.get(directory.getAbsolutePath()));
            } catch (IOException e) {
                log.info("the folder already exists.");
                throw new RuntimeException();
            }
        }
    }

    public byte[] getForUser(Long pictureId) {
        final UserAccount loggedUser = userService.getLoggedAccount();
        Picture imageForUser = pictureRepository.getByAddedBy_IdAndId(loggedUser.getId(), pictureId).orElseThrow();


        File image = new File(imageForUser.getPath());

        return getBytesForUserPicture(image);
    }

    public byte[] getBytesForUserPicture(File image) {
        try (InputStream stream = new FileInputStream(image)) {
            return StreamUtils.copyToByteArray(stream);
        } catch (IOException e) {
            throw new PictureException(e);
        }
    }

    public Picture savePicture(final MultipartFile multipartFile) {
        final UserAccount account = userService.getLoggedAccount();
        final String storagePath = storeFileOnDisk(multipartFile, account);

        final Picture picture = Picture.builder()
                .addedBy(account)
                .path(storagePath)
                .build();

        return pictureRepository.save(picture);
    }

    private String storeFileOnDisk(final MultipartFile multipartFile, final UserAccount account) {
        try {
            final String destination = determineDestination(account, multipartFile);
            log.info("Storing file to a new location: {}", destination);
            multipartFile.transferTo(new File(destination));
            return destination;
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String determineDestination(final UserAccount account, final MultipartFile multipartFile) {
        Path directoryPath = Paths.get(IMAGE_FOLDER_DIR, account.getId().toString());
        String identifier = UUID.randomUUID().toString();

        File finalDirectory = new File(directoryPath.toString());
        createDirectoryIfNotExists(finalDirectory);

        return String.join(File.separator,
                directoryPath.toAbsolutePath().toString(),
                identifier + "-" + multipartFile.getOriginalFilename());
    }

    @Transactional
    public void switchProfilePicture(final Long newProfilePicId) {
        final UserAccount loggedUser = userService.getLoggedAccount();

        unmarkProfilePictureIfExists(loggedUser);

        Picture newProfilePicture = pictureRepository.findById(newProfilePicId).orElseThrow();

        newProfilePicture.setProfile(true);

    }


    @Transactional
    public Picture saveProfilePicture(final MultipartFile profilePic) {
        final UserAccount account = userService.getLoggedAccount();
        final String storagePath = storeFileOnDisk(profilePic, account);

        unmarkProfilePictureIfExists(account);

        final Picture picture = Picture.builder()
                .addedBy(account)
                .path(storagePath)
                .isProfile(true)
                .build();

        return pictureRepository.save(picture);
    }

    public void unmarkProfilePictureIfExists(UserAccount account) {
        pictureRepository.findByAddedByAndIsProfile(account, true)
                .ifPresent(existingProfilePic -> {
                    existingProfilePic.setProfile(false);
                    pictureRepository.save(existingProfilePic);
                });
    }


    public void removePicture(Long pictureId) {
        UserAccount loggedAccount = userService.getLoggedAccount();
        Picture pictureToRemove = pictureRepository.getByAddedBy_IdAndId(loggedAccount.getId(), pictureId)
                .orElseThrow(() -> new MissingFileException("File Not found"));

        try {
            Files.delete(Paths.get(pictureToRemove.getPath()));
        } catch (IOException e) {
            throw new MissingFileException(e);
        }

        pictureRepository.delete(pictureToRemove);
    }

    public List<Long> getPicturesIdsForLoggedUser() {
        UserAccount loggedUser = userService.getLoggedAccount();
        final List<Picture> pictureList = pictureRepository.getByAddedBy_Id(loggedUser.getId());

        List<Long> picturesListIds = pictureList.stream()
                .map(Picture::getId)
                .collect(Collectors.toList());

        return picturesListIds;
    }


    public byte[] getProfilePictureForUser() {
        final UserAccount loggedUser = userService.getLoggedAccount();
        Picture currentProfilePic = pictureRepository.findByAddedByAndIsProfile(loggedUser, true)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Profile pic is not here"));

        File profileImage = new File(currentProfilePic.getPath());
        return getBytesForUserPicture(profileImage);

    }
}
