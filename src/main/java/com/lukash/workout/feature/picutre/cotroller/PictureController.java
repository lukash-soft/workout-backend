package com.lukash.workout.feature.picutre.cotroller;

import com.lukash.workout.feature.picutre.service.PictureService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("pictures")
@RequiredArgsConstructor
public class PictureController {

    private final PictureService pictureService;

    @GetMapping(produces = MediaType.IMAGE_PNG_VALUE, value  = "{pictureId}")
    public ResponseEntity<byte[]> getImageForUser(@PathVariable Long pictureId) {

        byte[] imageInBytes = pictureService.getForUser(pictureId);

        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_PNG)
                .body(imageInBytes);
    }

    @GetMapping
    public List<Long> getPicturesIdsForLoggedUser() {
        return pictureService.getPicturesIdsForLoggedUser();
    }

    @GetMapping(value = "profile", produces = MediaType.IMAGE_PNG_VALUE)
    public ResponseEntity<byte[]> getProfilePictureForUser() {

        byte[] profilePicInBytes = pictureService.getProfilePictureForUser();

        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_PNG)
                .body(profilePicInBytes);
    }

    @PostMapping("profile")
    public void uploadProfilePicture(@RequestParam("file") final MultipartFile profilePic) {
        pictureService.saveProfilePicture(profilePic);
    }

    @PutMapping("{pictureId}")
    public void switchProfilePicture(@PathVariable Long pictureId) {
        pictureService.switchProfilePicture(pictureId);
    }

    @PostMapping
    public void savePicture(@RequestParam("file") final MultipartFile picture) {
        pictureService.savePicture(picture);
    }

    @DeleteMapping("{pictureId}")
    public void removePicture(@PathVariable Long pictureId) {
        pictureService.removePicture(pictureId);
    }
}
