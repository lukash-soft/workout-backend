package com.lukash.workout.feature.picutre.repository;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.picutre.model.Picture;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PictureRepository extends JpaRepository<Picture, Long> {

    List<Picture> getByAddedBy_Id(Long accountId);
    Optional<Picture> getByAddedBy_IdAndId(Long accountId, Long pictureID);
    Optional<Picture> findByAddedByAndIsProfile(UserAccount userAccount, boolean isProfilePic);

}
