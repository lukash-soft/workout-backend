package com.lukash.workout.feature.picutre.exception;

public class PictureException extends RuntimeException {

    public PictureException(Throwable cause) {
        super(cause);
    }
}
