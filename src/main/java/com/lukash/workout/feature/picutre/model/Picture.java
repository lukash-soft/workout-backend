package com.lukash.workout.feature.picutre.model;

import com.lukash.workout.feature.account.model.UserAccount;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Picture {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @Schema(example = "/home/images")
    private String path;

    @Column(nullable = false)
    @Schema(example = "1900-01-20")
    @Builder.Default
    private LocalDateTime dateAdded = LocalDateTime.now();

    @Builder.Default
    private boolean isProfile = false;

    @ManyToOne(optional = false)
    private UserAccount addedBy;

}
