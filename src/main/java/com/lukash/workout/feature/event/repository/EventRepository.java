package com.lukash.workout.feature.event.repository;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.event.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

    List<Event> findByCreatedBy(UserAccount userAccount);
    Optional<Event> findByCreatedByIdAndId(Long creatorId, Long eventId);
    List<Event> findAllByParticipantsContaining(UserAccount participant) ;
}
