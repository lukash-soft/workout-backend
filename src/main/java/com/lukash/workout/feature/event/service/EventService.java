package com.lukash.workout.feature.event.service;

import com.lukash.workout.configuration.security.service.UserService;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.event.model.Event;
import com.lukash.workout.feature.event.repository.EventRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class EventService {


    private final EventRepository eventRepository;

    private final UserService userService;

    public List<Event> fetchForUser() {
        UserAccount loggedUser = userService.getLoggedAccount();

        List<Event> listOfUserAsCreator = eventRepository.findByCreatedBy(loggedUser);
        List<Event> listOfUserAsParticipant = eventRepository.findAllByParticipantsContaining(loggedUser);

        return Stream.concat(
                listOfUserAsCreator.stream(),
                listOfUserAsParticipant.stream()
        ).collect(Collectors.toList());
    }

    public Event createEvent(Event eventToCreate) {
        UserAccount loggedUser = userService.getLoggedAccount();
        eventToCreate.setCreatedBy(loggedUser);

        return eventRepository.save(eventToCreate);
    }

    public Event updateEvent(Event eventFromForm) {
        UserAccount loggedUser = userService.getLoggedAccount();
        Event existingEvent = eventRepository.findByCreatedByIdAndId(loggedUser.getId(), eventFromForm.getId()).orElseThrow();

        existingEvent.setDescription(eventFromForm.getDescription());
        existingEvent.setTitle(eventFromForm.getTitle());
        existingEvent.setEnd(eventFromForm.getEnd());
        existingEvent.setStart(eventFromForm.getStart());

        return eventRepository.save(existingEvent);
    }

    public void deleteEvent(Long eventId) {
        UserAccount loggedUser = userService.getLoggedAccount();
        Event eventToDelete = eventRepository.findByCreatedByIdAndId(loggedUser.getId(), eventId).orElseThrow();
        eventRepository.delete(eventToDelete);
    }


}
