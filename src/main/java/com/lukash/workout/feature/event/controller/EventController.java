package com.lukash.workout.feature.event.controller;

import com.lukash.workout.feature.event.dto.EventDto;
import com.lukash.workout.feature.event.model.Event;
import com.lukash.workout.feature.event.service.EventService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("event")
@RestController
@RequiredArgsConstructor
public class EventController {

    private final EventService eventService;

    @GetMapping()
    public List<EventDto> fetchForUser() {
        return eventService.fetchForUser().stream()
                .map(event -> EventDto.of(event))
                .collect(Collectors.toList());
    }

    @PostMapping
    public Event createEvent(@RequestBody Event eventToCreate) {
        return eventService.createEvent(eventToCreate);
    }

    @PutMapping
    public EventDto updateEvent(@RequestBody Event eventToUpdate) {
        Event eventUpdated = eventService.updateEvent(eventToUpdate);
        return EventDto.of(eventUpdated);
    }

    @DeleteMapping("{eventId}")
    public void deleteEvent(@PathVariable Long eventId) {
        eventService.deleteEvent(eventId);
    }
}
