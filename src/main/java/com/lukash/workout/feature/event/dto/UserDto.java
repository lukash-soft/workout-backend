package com.lukash.workout.feature.event.dto;

import com.lukash.workout.feature.account.model.UserAccount;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Long id;
    private String firstName;
    private String lastName;

    public static UserDto of(final UserAccount account) {
        return new UserDto(account.getId(), account.getFirstName(), account.getLastName());
    }

    public static List<UserDto> ofList(final List<UserAccount> userList) {
        List<UserDto> dtoList = new ArrayList<>();

        for (UserAccount account : userList) {
            dtoList.add(UserDto.of(account));
        }
        return dtoList;
    }

}
