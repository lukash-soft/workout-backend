package com.lukash.workout.feature.event.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.lukash.workout.feature.event.model.Event;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventDto {

    private Long id;
    private String description;
    private String title;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private OffsetDateTime start;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private OffsetDateTime end;
    private UserDto createdBy;
    private List<UserDto> participants;

    public static EventDto of(final Event event){
        return new EventDto(event.getId(), event.getDescription(), event.getTitle(), event.getStart(), event.getEnd(),
                UserDto.of(event.getCreatedBy()), UserDto.ofList(event.getParticipants()));
    }

    public static List<EventDto> ofList(final List<Event> eventList){
        List<EventDto> dtoEventList = new ArrayList<>();

        for(Event event : eventList){
            dtoEventList.add(EventDto.of(event));
        }
        return dtoEventList;
    }
}
