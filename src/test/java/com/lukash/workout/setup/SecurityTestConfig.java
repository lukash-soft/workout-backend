package com.lukash.workout.setup;

import com.lukash.workout.configuration.security.service.UserDatabaseDetailsService;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetailsService;

@TestConfiguration
@RequiredArgsConstructor
public class SecurityTestConfig {

    private final UserAccountRepository accountRepository;

    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        return new UserDatabaseDetailsService(accountRepository);
    }

}
