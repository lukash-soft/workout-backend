package com.lukash.workout.setup;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.athlete_details.model.AthleteDetails;
import com.lukash.workout.feature.event.model.Event;
import com.lukash.workout.feature.group.model.WorkoutGroup;
import com.lukash.workout.feature.invitation.model.Invitation;
import com.lukash.workout.feature.invitation.model.InvitationStatus;
import com.lukash.workout.feature.message.model.Message;
import com.lukash.workout.feature.offer.model.OfferModel;
import com.lukash.workout.feature.payment.invoicing.model.Invoice;
import com.lukash.workout.feature.payment.model.Payment;
import com.lukash.workout.feature.picutre.model.Picture;
import com.lukash.workout.feature.post.model.PostModel;
import com.lukash.workout.feature.subscription.model.Subscription;
import com.lukash.workout.feature.workoutPlan.dto.WorkoutDto;
import com.lukash.workout.feature.workoutPlan.model.Status;
import com.lukash.workout.feature.workoutPlan.model.WorkoutPlan;

import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Fixture {

    public static final String USER_EMAIL = "user@email";
    public static final String COACH_EMAIL = "coach@email.com";
    public static final String TEST_IMAGE_DIR = Paths.get("src", "test", "resources", "images").toAbsolutePath().toString();

    public Invoice prepareInvoice(final UserAccount userAccount) {
        return Invoice.builder()
                .user(userAccount)
                .totalAmount(32.0)
                .build();
    }

    public Payment preparePayment(final UserAccount userAccount) {
        return Payment.builder()
                .amount(65.0)
                .cardNumber("1234")
                .user(userAccount)
                .build();
    }

    public OfferModel prepareOffer(final UserAccount coach) {

        return OfferModel.builder()
                .coach(coach)
                .title("Test Title")
                .description("Test description")
                .price(20.0)
                .build();
    }

    public Subscription prepareSubscription(final UserAccount coach, final UserAccount client) {

        return Subscription.builder()
                .coach(coach)
                .client(client)
                .fee(50.0)
                .endDate(LocalDate.now().plusDays(31))
                .build();
    }

    public WorkoutPlan preparePlan(final UserAccount loggedUser) {
        List<WorkoutDto> workouts = getWorkouts();

        return WorkoutPlan.builder()
                .coach(loggedUser)
                .status(Status.READY)
                .workouts(workouts)
                .dateCreated(LocalDateTime.now())
                .build();
    }

    public WorkoutPlan preparePlan(final UserAccount loggedUser, final List<UserAccount> clients) {
        List<WorkoutDto> workouts = getWorkouts();

        return WorkoutPlan.builder()
                .coach(loggedUser)
                .status(Status.READY)
                .clients(clients)
                .workouts(workouts)
                .dateCreated(LocalDateTime.now())
                .build();
    }

    public List<WorkoutDto> getWorkouts() {
        List<WorkoutDto> workouts = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            workouts.add(WorkoutDto.builder()
                    .name("Test workout name " + i)
                    .details("Test Details " + i)
                    .build());
        }
        return workouts;
    }

    public UserAccount createClient() {
        return prepareAccount(USER_EMAIL, AccountRole.CLIENT);
    }

    public UserAccount createCoach() {
        return prepareAccount(COACH_EMAIL, AccountRole.COACH);
    }

    public UserAccount prepareAccount(final String email, final AccountRole accountRole) {
        return UserAccount.builder()
                .firstName("test")
                .lastName("test")
                .email(email)
                .password("password")
                .accountRole(accountRole)
                .birthDate(OffsetDateTime.now().minusYears(5))
                .build();
    }

    public List<UserAccount> prepareAccounts(final int numberOfAccounts) {
        //this is kind of for loop on steroids :D
        return IntStream.rangeClosed(1, numberOfAccounts)
                .mapToObj(shift -> UserAccount.builder()
                        .firstName("test" + shift)
                        .lastName("test" + shift)
                        .email("some@email" + shift)
                        .password("password")
                        .accountRole(AccountRole.CLIENT)
                        .birthDate(OffsetDateTime.now().minusYears(shift))
                        .build()
                ).collect(Collectors.toList());
    }

    public AthleteDetails prepareAthleteDetails(final UserAccount userAccount) {
        return AthleteDetails.builder()
                .weight(170)
                .squat(120)
                .height(170)
                .benchPress(120)
                .userAccount(userAccount)
                .build();
    }

    public Invitation prepareInvitation(final UserAccount userFrom, final UserAccount userTo) {

        return Invitation.builder()
                .accountFrom(userFrom)
                .accountTo(userTo)
                .created(LocalDateTime.now())
                .invitationStatus(InvitationStatus.PENDING)
                .edited(LocalDateTime.now())
                .build();
    }


    public List<WorkoutGroup> prepareGroups(UserAccount admin, final int numberOfGroups) {
        List<WorkoutGroup> groups = new ArrayList<>();

        for (int i = 0; i < numberOfGroups; i++) {
            final WorkoutGroup workoutGroup = new WorkoutGroup();

            workoutGroup.setGroupType("Test Group" + i);
            workoutGroup.setAdmin(admin);
            workoutGroup.setGroupName("Name" + i);
            groups.add(workoutGroup);
        }

        return groups;
    }

    public WorkoutGroup prepareGroupForAdmin(UserAccount admin) {

        return WorkoutGroup.builder()
                .groupName("Test Name")
                .groupType("Test type")
                .admin(admin)
                .build();
    }

    public List<Picture> preparePictures(UserAccount loggedUser, final int NumberOfPictures) {
        final List<Picture> pictures = new ArrayList<>();
        for (int i = 0; i < NumberOfPictures; i++) {
            Picture picture = new Picture();
            picture.setAddedBy(loggedUser);
            picture.setDateAdded(LocalDateTime.now().minusHours(2));
            picture.setPath(TEST_IMAGE_DIR + "/image.png");
            pictures.add(picture);
        }
        return pictures;
    }

    public Picture preparePicture(UserAccount loggedUser) {

        return Picture.builder()
                .addedBy(loggedUser)
                .path(TEST_IMAGE_DIR + "/image.png")
                .dateAdded(LocalDateTime.now().minusHours(2))
                .build();
    }

    public List<Message> prepareMessages(UserAccount userFrom, UserAccount userTo, int numberOfMessages) {
        List<Message> messagesList = new ArrayList<>();
        for (int i = 0; i < numberOfMessages; i++) {
            Message message = Message.builder()
                    .sender(userFrom)
                    .recipient(userTo)
                    .created(LocalDateTime.now())
                    .content("Something in the message")
                    .build();
            messagesList.add(message);
        }
        return messagesList;
    }

    public List<PostModel> preparePosts(UserAccount loggedUser, final int numberOfPosts) {
        final List<PostModel> postListsOfLoggedUser = new ArrayList<>();
        for (int i = 0; i < numberOfPosts; i++) {
            PostModel post = PostModel.builder()
                    .title("Test title")
                    .content("Test content")
                    .created(LocalDateTime.now())
                    .creator(loggedUser)
                    .build();
            postListsOfLoggedUser.add(post);
        }
        return postListsOfLoggedUser;
    }

    public PostModel preparePost(UserAccount loggedUser) {
        return PostModel.builder()
                .title("Test title")
                .content("Test content")
                .created(LocalDateTime.now())
                .creator(loggedUser)
                .build();
    }

    public Event prepareEvent(UserAccount loggedUser) {

        return Event.builder()
                .createdBy(loggedUser)
                .title("TestTitle")
                .description("This is test Event")
                .start(OffsetDateTime.now())
                .end(OffsetDateTime.now().plusHours(1))
                .participants(new ArrayList<>())
                .build();
    }

    public List<Event> prepareMultipleEvents(UserAccount loggedUser, int numberOFEvents) {
        List<Event> events = new ArrayList<>();

        for (int i = 0; i < numberOFEvents; i++) {
            events.add(Event.builder()
                    .createdBy(loggedUser)
                    .title("TestTitle of event nr. " + i)
                    .description("This is test Event nr. " + i)
                    .start(OffsetDateTime.now().plusHours(i))
                    .end(OffsetDateTime.now().plusHours(i))
                    .build());
        }
        return events;
    }

}
