package com.lukash.workout.configuration.security.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDate;
import java.time.ZoneId;

import static org.assertj.core.api.Assertions.assertThat;

class UserDatabaseDetailsServiceTest extends IntegrationTest {

    @Autowired
    UserAccountRepository userAccountRepository;
    @Autowired
    UserDatabaseDetailsService userDatabaseDetailsService;

    @AfterEach
    void cleanUp() {
        userAccountRepository.deleteAll();
    }

    @Test
    void shouldHaveActiveAccountWhenTerminatedIsToday() {
        //given
        final LocalDate terminatedDate = LocalDate.now();

        final UserAccount loggedUser = createUser(terminatedDate);
        userAccountRepository.save(loggedUser);

        //when
        UserDetails userDetails = userDatabaseDetailsService.loadUserByUsername(loggedUser.getEmail());

        //then
        assertThat(userDetails.isAccountNonExpired()).isTrue();
    }

    private UserAccount createUser(LocalDate terminatedDate) {
        LocalDate birthDay = LocalDate.of(1991, 11, 11);
        ZoneId zoneId = ZoneId.systemDefault();

        return UserAccount.builder()
                .birthDate(birthDay.atStartOfDay(zoneId).toOffsetDateTime())
                .firstName("Lukas")
                .lastName("Pietrynczak")
                .password("passtest")
                .created(LocalDate.of(2021, 5, 5))
                .terminatedDate(terminatedDate)
                .accountRole(AccountRole.COACH)
                .email("User@gmail.com")
                .build();
    }

    @Test
    void shouldHaveActiveAccountWhenTerminatedIsNull() {
        //given
        final UserAccount loggedUser = createUser(null);
        userAccountRepository.save(loggedUser);

        //when
        UserDetails userDetails = userDatabaseDetailsService.loadUserByUsername(loggedUser.getEmail());

        //then
        assertThat(userDetails.isAccountNonExpired()).isTrue();
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 5, 30})
    void shouldHaveActiveAccountWhenTerminatedIsInFuture(final int daysForward) {
        //given
        final LocalDate terminatedDate = LocalDate.now().plusDays(daysForward);

        final UserAccount loggedUser = createUser(terminatedDate);
        userAccountRepository.save(loggedUser);

        //when
        UserDetails userDetails = userDatabaseDetailsService.loadUserByUsername(loggedUser.getEmail());

        //then
        assertThat(userDetails.isAccountNonExpired()).isTrue();
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 5, 30})
    void shouldHaveAccountInactiveWhenTerminatedIsInPast(final int daysBackward) {
        //given
        final LocalDate terminatedDate = LocalDate.now().minusDays(daysBackward);

        final UserAccount loggedUser = createUser(terminatedDate);
        userAccountRepository.save(loggedUser);

        //when
        User user = (User) userDatabaseDetailsService.loadUserByUsername(loggedUser.getEmail());

        //then
        assertThat(user.isAccountNonExpired()).isFalse();
    }
}