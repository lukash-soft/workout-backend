package com.lukash.workout.feature.account.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.dto.RegistrationDto;
import com.lukash.workout.feature.account.dto.UserAccountDto;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.athlete_details.repository.AthleteDetailsRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class UserAccountServiceTest extends IntegrationTest {

    @Autowired
    UserAccountService userAccountService;

    @Autowired
    UserAccountRepository userAccountRepository;

    @Autowired
    AthleteDetailsRepository athleteDetailsRepository;

    final Fixture fixture = new Fixture();

    @AfterEach
    void cleanup() {
        athleteDetailsRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @Test
    void shouldGetAccounts() {
        //given
        final int numberOfAccounts = 3;
        userAccountRepository.saveAll(fixture.prepareAccounts(numberOfAccounts));
        // when
        final List<UserAccountDto> result = userAccountService.getAccounts();
        //then
        assertThat(result).hasSize(numberOfAccounts);
    }

    @Test
    void shouldGetAllClients() {
        //given
        final int numberOfAccounts = 3;
        userAccountRepository.saveAll(fixture.prepareAccounts(numberOfAccounts));
        //when
        final List<UserAccountDto> clients = userAccountService.getClients();
        //then
        assertThat(clients).hasSize(numberOfAccounts);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetUserDetails() {
        //given
        userAccountRepository.save(fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH));
        // when
        final UserAccountDto loggedUser = userAccountService.getUserDetails();
        //then
        assertThat(loggedUser.getEmail()).isEqualTo(Fixture.USER_EMAIL);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldUpdateAccount() {
        //given
        UserAccountDto originalUser = UserAccountDto.of(
                userAccountRepository.save(fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH)));

        LocalDate birthDay = LocalDate.of(1991, 11, 11);
        ZoneId zoneId = ZoneId.systemDefault();

        final UserAccountDto userAccountDtoFromForm = UserAccountDto.builder()
                .firstName("Updated first name")
                .lastName("Updated last name")
                .birthDate(birthDay.atStartOfDay(zoneId).toOffsetDateTime())
                .build();

        // when
        originalUser = userAccountService.updateAccount(userAccountDtoFromForm);
        //then
        assertThat(originalUser.getFirstName()).isEqualTo(userAccountDtoFromForm.getFirstName());
        assertThat(originalUser.getLastName()).isEqualTo(userAccountDtoFromForm.getLastName());
        assertThat(originalUser.getBirthDate()).isEqualTo(userAccountDtoFromForm.getBirthDate());
        assertThat(originalUser.getEmail()).isSameAs(Fixture.USER_EMAIL);
        assertThat(originalUser.getId()).isEqualTo(originalUser.getId());
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldTestUpdatingBio() {
        //given
        UserAccountDto loggedUser =
                UserAccountDto.of(
                        userAccountRepository.save(
                                fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH)));

        final UserAccountDto userAccountDtoFromForm = UserAccountDto.builder()
                .id(loggedUser.getId())
                .bio("This is test bio")
                .build();

        //when
        userAccountService.updateBio(userAccountDtoFromForm);

        //then
        UserAccount loggedUserAfterUpdate = userAccountRepository.findByEmail(loggedUser.getEmail()).get();
        assertThat(loggedUserAfterUpdate.getBio()).isEqualTo(userAccountDtoFromForm.getBio());
    }

    @Transactional
    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldRemoveAccount() {
        //given

        final UserAccount userToRemove = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(userToRemove);

        // when
        userAccountService.removeAccount();
        //then
        final List<UserAccount> usersAfterRemoval = userAccountRepository.findAll();
        assertThat(usersAfterRemoval).doesNotContain(userToRemove);
    }

    @Test
    void shouldRegisterAccount() {
        //given
        LocalDate birthDay = LocalDate.of(1991, 11, 11);
        ZoneId zoneId = ZoneId.systemDefault();

        final RegistrationDto registrationDto = RegistrationDto.builder()
                .firstName("name")
                .lastName("lastName")
                .birthDate(birthDay.atStartOfDay(zoneId).toOffsetDateTime())
                .password("PASSWORD")
                .email("some@email")
                .role(AccountRole.RoleValues.ROLE_COACH)
                .build();

        //when
        userAccountService.registerAccount(registrationDto);

        //then
        final UserAccount saved = userAccountRepository.findByEmail(registrationDto.getEmail()).get();
        assertThat(saved.getEmail()).isEqualTo(registrationDto.getEmail());
    }

    @Test
    void shouldThrowExceptionWhenBirthDateIsInTheFuture() {
        LocalDate birthDay = LocalDate.now().plusDays(1);
        ZoneId zoneId = ZoneId.systemDefault();

        final RegistrationDto registrationDto = RegistrationDto.builder()
                .firstName("name")
                .lastName("lastName")
                .birthDate(birthDay.atStartOfDay(zoneId).toOffsetDateTime())
                .password("PASSWORD")
                .email("some@email")
                .role(AccountRole.RoleValues.ROLE_COACH)
                .build();

        //when, then
        assertThatThrownBy(() -> userAccountService.registerAccount(registrationDto)).isInstanceOf(RuntimeException.class);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetAllNonConnectedUsers() {
        //given
        int totalNumberOfAccounts = 3;
        final List<UserAccount> accounts = userAccountRepository.saveAll(fixture.prepareAccounts(totalNumberOfAccounts));
        final UserAccount friend = accounts.get(0);

        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        loggedUser.setFriends(List.of(friend));
        userAccountRepository.save(loggedUser);

        // when
        final List<UserAccountDto> result = userAccountService.getAllNonConnectedUsers();
        //then

        assertThat(result)
                .hasSize(2)
                .extracting(UserAccountDto::getEmail).doesNotContain(friend.getEmail());
    }

    @Test
    @Transactional
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetAllFriends() {
        //given
        int numberOfFriends = 3;
        final List<UserAccount> accounts = userAccountRepository.saveAll(fixture.prepareAccounts(numberOfFriends));

        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        loggedUser.setFriends(accounts);
        userAccountRepository.save(loggedUser);
        // when
        final List<UserAccountDto> friendsList = userAccountService.getAllFriends();
        //then
        assertThat(friendsList).hasSize(numberOfFriends);
    }


    @Test
    void shouldThrowExceptionWhenFirstNameIsNull() {
        // given
        LocalDate birthDay = LocalDate.of(1991, 11, 11);
        ZoneId zoneId = ZoneId.systemDefault();
        final RegistrationDto registrationDto = RegistrationDto.builder()
                .firstName(null)
                .lastName("lastname")
                .birthDate(birthDay.atStartOfDay(zoneId).toOffsetDateTime())
                .password("Pass")
                .email("String@gmail.com")
                .build();

        // when, then
        assertThatThrownBy(() -> userAccountService.registerAccount(registrationDto)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void shouldThrowExceptionWhenLastNameIsBlank() {
        //given
        LocalDate birthDay = LocalDate.of(1991, 11, 11);
        ZoneId zoneId = ZoneId.systemDefault();

        final RegistrationDto registrationDto = RegistrationDto.builder()
                .firstName("Name")
                .lastName(null)
                .birthDate(birthDay.atStartOfDay(zoneId).toOffsetDateTime())
                .password("Pass")
                .email("String@gmail.com")
                .build();
        //when, then
        assertThatThrownBy(() -> userAccountService.registerAccount(registrationDto)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void shouldThrowExceptionWhenEmailIsBlank() {
        //given
        LocalDate birthDay = LocalDate.of(1991, 11, 11);
        ZoneId zoneId = ZoneId.systemDefault();

        final RegistrationDto registrationDto = RegistrationDto.builder()
                .firstName("Name")
                .lastName("lastName")
                .birthDate(birthDay.atStartOfDay(zoneId).toOffsetDateTime())
                .password("Pass")
                .email(null)
                .build();

        //when, then
        assertThatThrownBy(() -> userAccountService.registerAccount(registrationDto)).isInstanceOf(RuntimeException.class);
    }


    @Test
    void shouldThrowExceptionWhenBirthDateIsPresent() {
        //given
        final RegistrationDto registrationDto = RegistrationDto.builder()
                .firstName("Name")
                .lastName("lastName")
                .birthDate(null)
                .password("Pass")
                .email("String@gmail.com")
                .build();
        //when, then
        assertThatThrownBy(() -> userAccountService.registerAccount(registrationDto)).isInstanceOf(RuntimeException.class);
    }
}
