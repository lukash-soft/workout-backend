package com.lukash.workout.feature.account.controller;

import com.lukash.workout.feature.account.dto.UserAccountDto;
import com.lukash.workout.feature.account.service.UserAccountService;
import com.lukash.workout.setup.Fixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserAccountControllerTest {

    final Fixture fixture = new Fixture();

    @Mock
    UserAccountService userAccountService;
    @InjectMocks
    UserAccountController userAccountController;

    @Test
    void getAllNonConnectedUsers() {
        //given, when
        userAccountController.getAllNonConnectedUsers();

        //then
        verify(userAccountService, times(1)).getAllNonConnectedUsers();
    }

    @Test
    void getFriends() {
        //given, when
        userAccountController.getFriends();

        //then
        verify(userAccountService, times(1)).getAllFriends();
    }

    @Test
    void getAllAccounts() {
        //given, when
        userAccountController.getAllAccounts();

        //then
        verify(userAccountService, times(1)).getAccounts();
    }

    @Test
    void getAccountDetails() {
        //given, when
        userAccountController.getAccountDetails();

        //then
        verify(userAccountService, times(1)).getUserDetails();
    }

    @Test
    void updateAccount() {
        //given
        final UserAccountDto user = UserAccountDto.of(fixture.createClient());

        //when
        userAccountController.updateAccount(user);

        //then
        verify(userAccountService, times(1)).updateAccount(user);
    }

    @Test
    void updateBio() {
        //given
        final UserAccountDto user = UserAccountDto.of(fixture.createClient());

        //when
        userAccountController.updateBio(user);

        //then
        verify(userAccountService, times(1)).updateBio(user);

    }

    @Test
    void removeAccount() {
        //given
        final UserAccountDto user = UserAccountDto.of(fixture.createClient());

        //when
        userAccountController.removeAccount();

        //then
        verify(userAccountService, times(1)).removeAccount();

    }
}