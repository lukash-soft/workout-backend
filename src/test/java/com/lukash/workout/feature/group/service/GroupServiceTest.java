package com.lukash.workout.feature.group.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.group.model.WorkoutGroup;
import com.lukash.workout.feature.group.repository.GroupRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class GroupServiceTest extends IntegrationTest {
    @Autowired
    UserAccountRepository userAccountRepository;
    @Autowired
    GroupRepository groupRepository;
    @Autowired
    GroupService groupService;

    Fixture fixture = new Fixture();

    @AfterEach
    void cleanup() {
        groupRepository.deleteAll();
        userAccountRepository.deleteAll();
    }


    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void getAllForUser() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final WorkoutGroup groupList = fixture.prepareGroupForAdmin(loggedUser);
        groupRepository.save(groupList);

        //when
        final List<WorkoutGroup> userGroupList = groupService.getAllForLoggedUser();
        //then
        assertThat(userGroupList).hasSize(1);
    }

    @Test
    @Transactional
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetMembersOfAGroup() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);

        final WorkoutGroup chosenGroup = fixture.prepareGroupForAdmin(loggedUser);
        final List<UserAccount> members = fixture.prepareAccounts(4);
        userAccountRepository.saveAll(members);
        chosenGroup.setMembers(members);
        groupRepository.save(chosenGroup);

        //when
        final List<UserAccount> chosenGroupMembers = groupService.getMembersForGroup(chosenGroup.getId());
        //then
        assertThat(chosenGroupMembers).hasSize(members.size());

    }

    @Transactional
    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldAddMember() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);

        final WorkoutGroup chosenGroup = fixture.prepareGroupForAdmin(loggedUser);
        groupRepository.save(chosenGroup);
        int initialChosenGroupSize = chosenGroup.getMembers().size();

        final UserAccount testFutureMember = fixture.prepareAccount("testMember@gmail.com", AccountRole.CLIENT);
        userAccountRepository.save(testFutureMember);

        //when
        groupService.addMember(testFutureMember.getId(), chosenGroup.getId());
        //then
        final WorkoutGroup groupAfterAdding = groupRepository.findById(chosenGroup.getId()).get();
        assertThat(groupAfterAdding.getMembers().size()).isEqualTo(initialChosenGroupSize + 1);
    }


    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldThrowExceptionWhenUserIsAlreadyMember(){
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);

        final WorkoutGroup chosenGroup = fixture.prepareGroupForAdmin(loggedUser);
        groupRepository.save(chosenGroup);

        final UserAccount testFutureMember = fixture.prepareAccount("testMember@gmail.com", AccountRole.CLIENT);
        userAccountRepository.save(testFutureMember);

        groupService.addMember(testFutureMember.getId(), chosenGroup.getId());
        groupRepository.save(chosenGroup);

        //when, then
        assertThatThrownBy(() -> groupService.addMember(testFutureMember.getId(), chosenGroup.getId())).isInstanceOf(RuntimeException.class);
    }

    @Transactional
    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void removeMember() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        userAccountRepository.save(loggedUser);

        final WorkoutGroup chosenGroup = fixture.prepareGroupForAdmin(loggedUser);
        int initialChosenGroupSize = 3;
        final List<UserAccount> members = fixture.prepareAccounts(initialChosenGroupSize);
        userAccountRepository.saveAll(members);
        chosenGroup.setMembers(members);
        groupRepository.save(chosenGroup);

        // when
        groupService.removeMember(chosenGroup.getMembers().get(0).getId(), chosenGroup.getId());
        //then
        final int afterAddingGroupSize = chosenGroup.getMembers().size();
        assertThat(afterAddingGroupSize).isEqualTo(initialChosenGroupSize - 1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void updateGroup() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);


        final WorkoutGroup chosenGroup = fixture.prepareGroupForAdmin(loggedUser);
        groupRepository.save(chosenGroup);

        WorkoutGroup fromForm = WorkoutGroup.builder()
                .id(chosenGroup.getId())
                .groupName("Updated Group")
                .groupType("Updated type group")
                .build();


        //when
        groupService.updateGroup(fromForm);
        //then
        final WorkoutGroup workoutGroupAfterUpdate = groupRepository.findById(chosenGroup.getId()).get();
        assertThat(chosenGroup.getGroupName()).isNotEqualTo(workoutGroupAfterUpdate.getGroupName());
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void saveGroup() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);

        final List<WorkoutGroup> groupList = fixture.prepareGroups(loggedUser, 1);
        groupRepository.saveAll(groupList);

        final WorkoutGroup groupToSave = WorkoutGroup.builder()
                .admin(loggedUser)
                .groupName("Strength to save")
                .groupType("test group to save")
                .build();

        int initialGroupQuantityForLoggedUser = groupRepository.findByAdmin_Id(loggedUser.getId()).size();
        int initialGroupQuantity = groupRepository.findAll().size();
        //when
        groupService.saveGroup(groupToSave);
        //then
        final List<WorkoutGroup> allGroupsOfLoggedUser = groupRepository.findByAdmin_Id(loggedUser.getId());
        int groupQuantityAfterAdding = groupRepository.findAll().size();

        assertThat(allGroupsOfLoggedUser).hasSize(initialGroupQuantityForLoggedUser + 1);
        assertThat(groupQuantityAfterAdding).isEqualTo(initialGroupQuantity + 1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void removeGroupOfUser() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);

        final WorkoutGroup groupToRemove = fixture.prepareGroupForAdmin(loggedUser);
        groupRepository.save(groupToRemove);
        final List<WorkoutGroup> groupListBeforeRemoval = groupRepository.findAll();

        //when
        groupService.removeGroup(groupToRemove.getId());
        //then
        final List<WorkoutGroup> groupListAfterRemoval = groupRepository.findAll();

        assertThat(groupListAfterRemoval).hasSize(groupListBeforeRemoval.size() - 1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldThrowExceptionWhenGroupNameIsNull() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final List<UserAccount> members = fixture.prepareAccounts(5);
        userAccountRepository.saveAll(members);

        WorkoutGroup groupToTest = WorkoutGroup.builder()
                .groupName(null)
                .admin(loggedUser)
                .members(members)
                .groupType("Strength")
                .build();
        //when, then
        assertThatThrownBy(() -> groupService.saveGroup(groupToTest)).isInstanceOf(RuntimeException.class);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldThrowExceptionWhenGroupTypeIsNull() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final List<UserAccount> members = userAccountRepository.saveAll(fixture.prepareAccounts(5));

        WorkoutGroup groupToTest = WorkoutGroup.builder()
                .groupName("Group1")
                .admin(loggedUser)
                .members(members)
                .groupType(null)
                .build();
        //when, then
        assertThatThrownBy(() -> groupService.saveGroup(groupToTest)).isInstanceOf(RuntimeException.class);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldThrowExceptionWhenAdminIsNull() {
        //given
        final List<UserAccount> members = userAccountRepository.saveAll(fixture.prepareAccounts(5));

        WorkoutGroup groupToTest = WorkoutGroup.builder()
                .groupName("Group1")
                .admin(null)
                .members(members)
                .groupType("Strength")
                .build();
        //when, then
        assertThatThrownBy(() -> groupService.saveGroup(groupToTest)).isInstanceOf(RuntimeException.class);
    }
}
