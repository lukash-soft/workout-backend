package com.lukash.workout.feature.group.controller;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.group.model.WorkoutGroup;
import com.lukash.workout.feature.group.service.GroupService;
import com.lukash.workout.setup.Fixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class GroupControllerUnitTest {

    final Fixture fixture = new Fixture();

    @Mock
    GroupService groupService;
    @InjectMocks
    GroupController groupController;

    @Test
    void getAll() {
        //given, when
        groupController.getAll();

        //then
        verify(groupService, times(1)).getAll();

    }

    @Test
    void getAllForLoggedUser() {
        //given, when
        groupController.getAllForLoggedUser();

        //then
        verify(groupService, times(1)).getAllForLoggedUser();
    }

    @Test
    void saveGroup() {
        //given
        final WorkoutGroup workoutGroup = createMockGroup();

        //when
        groupController.saveGroup(workoutGroup);

        //then
        verify(groupService, times(1)).saveGroup(workoutGroup);
    }



    @Test
    void updateGroup() {
        //given
        final WorkoutGroup workoutGroup = createMockGroup();

        //when
        groupController.updateGroup(workoutGroup);

        //then
        verify(groupService, times(1)).updateGroup(workoutGroup);
    }

    @Test
    void getMembersForGroup() {
    }

    @Test
    void addMember() {
        //given
        final WorkoutGroup workoutGroup = createMockGroup();
        final UserAccount user = fixture.createClient();

        //when
        groupController.addMember(user.getId() , workoutGroup.getId());

        //then
        verify(groupService, times(1)).addMember(user.getId(), workoutGroup.getId());
    }

    @Test
    void removeMember() {
        //given
        final WorkoutGroup workoutGroup = createMockGroup();
        final UserAccount user = fixture.createClient();

        //when
        groupController.removeMember(user.getId(), workoutGroup.getId());

        //then
        verify(groupService, times(1)).removeMember(user.getId(), workoutGroup.getId());
    }

    @Test
    void removeGroup() {
        //given
        final WorkoutGroup workoutGroup = createMockGroup();

        //when
        groupController.removeGroup(workoutGroup.getId());

        //then
        verify(groupService, times(1)).removeGroup(workoutGroup.getId());
    }

    private WorkoutGroup createMockGroup() {
        return fixture.prepareGroupForAdmin(fixture.createCoach());
    }
}