package com.lukash.workout.feature.subscription.controller;

import com.lukash.workout.feature.offer.dto.OfferDto;
import com.lukash.workout.feature.offer.model.OfferModel;
import com.lukash.workout.feature.offer.service.OfferService;
import com.lukash.workout.feature.subscription.dto.SubscriptionDto;
import com.lukash.workout.feature.subscription.model.Subscription;
import com.lukash.workout.feature.subscription.service.SubscriptionService;
import com.lukash.workout.setup.Fixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SubscriptionControllerUnitTest {

    final Fixture fixture = new Fixture();

    @Mock
    SubscriptionService subscriptionService;
    @Mock
    OfferService offerService;
    @InjectMocks
    SubscriptionController subscriptionController;

    @Test
    void shouldGetSubscriptionsForUser() {
        //when
        final Subscription mockResponse = createMockSubscription();

        when(subscriptionService.getAllSubscriptionsForUser()).thenReturn(List.of(SubscriptionDto.of(mockResponse)));

        //given
        final List<SubscriptionDto> result = subscriptionController.getSubscriptionsForUser();

        //then
        assertThat(result)
                .hasSize(1)
                .extracting(SubscriptionDto::getId)
                .containsOnly(mockResponse.getId());
    }

    private Subscription createMockSubscription() {
        return fixture.prepareSubscription(fixture.createCoach(), fixture.createClient());
    }

    @Test
    void shouldCreateSubscription() {
        //when
        final OfferDto offerDto = OfferDto.of(createMockOffer());

        //given
        subscriptionController.createSubscription(offerDto);

        //then
        verify(subscriptionService, times(1)).createSubscription(eq(offerDto));
    }

    private OfferModel createMockOffer() {
        return fixture.prepareOffer(fixture.createCoach());
    }

    @Test
    void shouldUpdateSubscriptionFee() {
        //when
        final SubscriptionDto existingSubscription = SubscriptionDto.of(createMockSubscription());

        when(subscriptionService.updateSubscriptionFee(any())).thenReturn(existingSubscription);

        //given
        subscriptionController.updateSubscriptionFee(existingSubscription);

        //then
        verify(subscriptionService, times(1)).updateSubscriptionFee(eq(existingSubscription));
    }

    @Test
    void shouldCancelSubscription() {
        //given
        final Subscription subscriptionToCancel = createMockSubscription();

        when(subscriptionService.cancelSubscription(subscriptionToCancel.getId())).thenReturn(SubscriptionDto.of(subscriptionToCancel));

        //when
        subscriptionController.cancelSubscription(subscriptionToCancel.getId());

        //then
        verify(subscriptionService, times(1)).cancelSubscription(subscriptionToCancel.getId());
    }

    @Test
    void shouldRemoveSubscription() {
        //given
        final SubscriptionDto subToRemove = SubscriptionDto.of(createMockSubscription());

        //when
        subscriptionController.removeSubscription(subToRemove.getId());

        //then
        verify(subscriptionService, times(1)).removeSubscription(subToRemove.getId());
    }


}
