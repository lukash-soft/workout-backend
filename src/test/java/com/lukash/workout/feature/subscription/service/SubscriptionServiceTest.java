package com.lukash.workout.feature.subscription.service;

import com.lukash.workout.feature.account.dto.UserAccountDto;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.offer.dto.OfferDto;
import com.lukash.workout.feature.offer.dto.OfferUserDto;
import com.lukash.workout.feature.offer.repository.OfferRepository;
import com.lukash.workout.feature.subscription.dto.SubscriptionDto;
import com.lukash.workout.feature.subscription.model.Subscription;
import com.lukash.workout.feature.subscription.repository.SubscriptionRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SubscriptionServiceTest extends IntegrationTest {

    @Autowired
    UserAccountRepository userAccountRepository;
    @Autowired
    SubscriptionRepository subscriptionRepository;
    @Autowired
    SubscriptionService subscriptionService;
    @Autowired
    OfferRepository offerRepository;

    Fixture fixture = new Fixture();

    @AfterEach
    void cleanUp() {
        subscriptionRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetAllSubscriptionsForLoggedUserAsCoach() {
        //given
        final UserAccount loggedUserAsCoach = fixture.createCoach();
        final UserAccount client = fixture.createClient();
        userAccountRepository.save(client);
        userAccountRepository.save(loggedUserAsCoach);

        Subscription subscription = fixture.prepareSubscription(loggedUserAsCoach, client);
        subscriptionRepository.save(subscription);

        //when
        List<SubscriptionDto> subscriptionsOfCoach = subscriptionService.getAllSubscriptionsForUser();

        //then
        assertThat(subscriptionsOfCoach).hasSize(1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetAllSubscriptionsForLoggedUserAsClient() {
        //given
        final UserAccount coach = fixture.createCoach();
        final UserAccount client = fixture.createClient();
        userAccountRepository.save(client);
        userAccountRepository.save(coach);

        Subscription subscription = fixture.prepareSubscription(coach, client);
        subscriptionRepository.save(subscription);

        //when
        List<SubscriptionDto> subscriptionsOfClient = subscriptionService.getAllSubscriptionsForUser();

        //then
        assertThat(subscriptionsOfClient).hasSize(1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldCreateSubscription() {
        //given
        final UserAccount client = fixture.createClient();
        userAccountRepository.save(client);
        final UserAccount coach = fixture.createCoach();
        userAccountRepository.save(coach);

        OfferDto offerDto = OfferDto.builder()
                .title("Test")
                .coach(OfferUserDto.of(coach))
                .description("Desc test")
                .price(65.0)
                .build();

        //when
        subscriptionService.createSubscription(offerDto);

        //then
        List<Subscription> subscriptionsOfLoggedUser = subscriptionRepository.findByCoach(coach);
        assertThat(subscriptionsOfLoggedUser).hasSize(1);
    }


    @Test
    @WithMockUser(Fixture.COACH_EMAIL)
    void shouldCancelSubscription() {
        //given
        final UserAccount loggedUser = fixture.createCoach();
        userAccountRepository.save(loggedUser);
        final UserAccount client = fixture.createClient();
        userAccountRepository.save(client);
        final Subscription createdSubscription = fixture.prepareSubscription(loggedUser, client);
        subscriptionRepository.save(createdSubscription);

        //when
        subscriptionService.cancelSubscription(createdSubscription.getId());

        //then
        Subscription updatedSubscription = subscriptionRepository.findById(createdSubscription.getId()).get();

        assertThat(updatedSubscription.getEndDate()).isEqualTo(LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()));
    }

    @Test
    @WithMockUser(Fixture.COACH_EMAIL)
    void shouldUpdateSubscription() {
        //given
        final UserAccount loggedUser = fixture.createCoach();
        userAccountRepository.save(loggedUser);
        final UserAccount client = fixture.createClient();
        userAccountRepository.save(client);
        final Subscription createdSubscription = fixture.prepareSubscription(loggedUser, client);
        subscriptionRepository.save(createdSubscription);

        SubscriptionDto fromForm = SubscriptionDto.builder()
                .id(createdSubscription.getId())
                .coach(UserAccountDto.of(loggedUser))
                .client(UserAccountDto.of(client))
                .fee(90.6)
                .ended(LocalDate.now().plusMonths(5))
                .build();
        assertThat(createdSubscription.getFee()).isNotEqualTo(fromForm.getFee());

        //when
        subscriptionService.updateSubscriptionFee(fromForm);

        //then
        Subscription updatedSubscription = subscriptionRepository.findById(createdSubscription.getId()).get();

        assertThat(updatedSubscription.getFee()).isEqualTo(fromForm.getFee());
    }

    @Test
    @WithMockUser(Fixture.COACH_EMAIL)
    void shouldRemoveSubscription() {
        //given
        final UserAccount loggedUser = fixture.createCoach();
        userAccountRepository.save(loggedUser);
        final UserAccount client = fixture.createClient();
        userAccountRepository.save(client);
        final Subscription createdSubscription = fixture.prepareSubscription(loggedUser, client);
        subscriptionRepository.save(createdSubscription);
        List<Subscription> subscriptionsBeforeRemoval = subscriptionRepository.findByCoach(loggedUser);

        //when
        subscriptionService.removeSubscription(createdSubscription.getId());

        //then
        List<Subscription> subscriptionsAfterRemoval = subscriptionRepository.findByCoach(loggedUser);
        assertThat(subscriptionsAfterRemoval).hasSize(subscriptionsBeforeRemoval.size() - 1);

    }
}