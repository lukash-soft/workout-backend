package com.lukash.workout.feature.post.controller;

import com.lukash.workout.feature.post.model.PostModel;
import com.lukash.workout.feature.post.service.PostService;
import com.lukash.workout.setup.Fixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PostControllerUnitTest {

    final Fixture fixture = new Fixture();

    @Mock
    PostService postService;
    @InjectMocks
    PostController postController;

    @Test
    void shouldGetAll() {
        //given
        final PostModel mock = createMockPost();
        when(postService.getAll()).thenReturn(List.of(mock));

        //when
        List<PostModel> posts = postController.shouldGetAll();

        //then
        verify(postService, times(1)).getAll();
        assertThat(posts).hasSize(1)
                .extracting((id) -> mock.getId())
                .containsOnly(mock.getId());
    }

    @Test
    void shouldGetAllForUser() {
        //given, when
        postController.shouldGetAllForUser();

        //then
        verify(postService, times(1)).getAllForUser();
    }

    @Test
    void shouldSavePost() {
        //given
        final PostModel mock = createMockPost();

        //when
        postController.savePost(mock);

        //then
        verify(postService, times(1)).savePost(mock);
    }

    @Test
    void shouldUpdatePost() {
        //given
        final PostModel mock = createMockPost();

        //when
        postController.updatePost(mock);

        //then
        verify(postService, times(1)).updatePost(mock);
    }

    @Test
    void shouldRemovePost() {
        //given
        final PostModel mock = createMockPost();

        //when
        postController.removePost(mock.getId());

        //then
        verify(postService, times(1)).removePost(mock.getId());
    }

    private PostModel createMockPost() {
        return fixture.preparePost(fixture.createCoach());
    }
}