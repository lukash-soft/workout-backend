package com.lukash.workout.feature.post.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.post.model.PostModel;
import com.lukash.workout.feature.post.repository.PostRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


class PostServiceTest extends IntegrationTest {

    @Autowired
    PostRepository postRepository;
    @Autowired
    PostService postService;
    @Autowired
    UserAccountRepository userAccountRepository;

    Fixture fixture = new Fixture();

    //this method will be run after each test
    @AfterEach
    void cleanup() {
        postRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @Test
    void getAll() {
        //given, when
        List<PostModel> postModelList = postService.getAll();
        //then
        assertThat(postModelList).hasSize(postRepository.findAll().size());
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void getAllForUser() {
        //given
        final int numberOfPosts = 4;
        final UserAccount loggedAccount = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedAccount);
        final List<PostModel> posts = fixture.preparePosts(loggedAccount, numberOfPosts);
        postRepository.saveAll(posts);
        //when
        final List<PostModel> postModelOfUser = postService.getAllForUser();
        //then
        assertThat(postModelOfUser).hasSize(postRepository.findAllByCreatorId(loggedAccount.getId()).size());
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void updatePost() {
        //given
        final UserAccount loggedAccount = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedAccount);
        PostModel postBeforeUpdate = fixture.preparePost(loggedAccount);
        postRepository.save(postBeforeUpdate);

        PostModel fromForm = PostModel.builder()
                .id(postBeforeUpdate.getId())
                .creator(loggedAccount)
                .title("Updated Title")
                .content("Updated Content")
                .build();
        //when
        postService.updatePost(fromForm);
        //then
        PostModel postAfterUpdate = postRepository.findById(postBeforeUpdate.getId()).get();

        assertThat(postBeforeUpdate.getTitle()).isNotEqualTo(postAfterUpdate.getTitle());
        assertThat(postBeforeUpdate.getContent()).isNotEqualTo(postAfterUpdate.getContent());
        assertThat(postBeforeUpdate.getCreator().getId()).isEqualTo(postAfterUpdate.getCreator().getId());
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void savePost() {
        //given
        final UserAccount loggedAccount = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        userAccountRepository.save(loggedAccount);
        final int userPostsBeforeCreating = postRepository.findAllByCreatorId(loggedAccount.getId()).size();
        final PostModel postModelToSave = PostModel.builder()
                .title("Test Post to save Title")
                .content("Test Post to save content")
                .creator(loggedAccount)
                .created(LocalDateTime.now())
                .build();
        //when
        postService.savePost(postModelToSave);
        //then
        final List<PostModel> userPostsAfterCreating = postRepository.findAllByCreatorId(loggedAccount.getId());
        assertThat(userPostsAfterCreating).hasSize(userPostsBeforeCreating + 1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldThrowExceptionWhenPostContentIsEmpty(){
        //given
        final UserAccount loggedAccount = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        userAccountRepository.save(loggedAccount);
        final PostModel postModelToSave = PostModel.builder()
                .title("Test Post to save Title")
                .content("")
                .creator(loggedAccount)
                .created(LocalDateTime.now())
                .build();

        //when,then
        assertThatThrownBy(() -> postService.savePost(postModelToSave)).isInstanceOf(RuntimeException.class);

    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void removePost() {
        //given
        final int numberOfPosts = 4;
        final UserAccount loggedAccount = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        userAccountRepository.save(loggedAccount);
        final List<PostModel> posts = fixture.preparePosts(loggedAccount, numberOfPosts);
        postRepository.saveAll(posts);

        final PostModel postToRemove = postRepository.findAllByCreatorId(loggedAccount.getId()).get(0);
        //when
        postService.removePost(postToRemove.getId());
        //then
        final List<PostModel> userPostsAfterRemoving = postRepository.findAllByCreatorId(loggedAccount.getId());
        assertThat(userPostsAfterRemoving).hasSize(posts.size() - 1);
    }

}
