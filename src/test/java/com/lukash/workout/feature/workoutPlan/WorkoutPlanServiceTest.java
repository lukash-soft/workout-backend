package com.lukash.workout.feature.workoutPlan;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.dto.UserAccountDto;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.workoutPlan.dto.WorkoutPlanDto;
import com.lukash.workout.feature.workoutPlan.model.Status;
import com.lukash.workout.feature.workoutPlan.model.WorkoutPlan;
import com.lukash.workout.feature.workoutPlan.repository.WorkoutPlanRepository;
import com.lukash.workout.feature.workoutPlan.service.WorkoutPlanService;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class WorkoutPlanServiceTest extends IntegrationTest {

    @Autowired
    private WorkoutPlanRepository workoutPlanRepository;
    @Autowired
    private UserAccountRepository userAccountRepository;
    @Autowired
    private WorkoutPlanService workoutPlanService;

    Fixture fixture = new Fixture();

    @AfterEach
    void cleanUp() {
        workoutPlanRepository.deleteAll();
        userAccountRepository.deleteAll();
    }


    @Test
    @Transactional
    @WithMockUser(Fixture.COACH_EMAIL)
    void shouldGetWorkoutPlanWhenLoggedUserIsCoach() {
        //given
        UserAccount loggedUser = fixture.createCoach();
        userAccountRepository.save(loggedUser);

        WorkoutPlan workoutPlan = fixture.preparePlan(loggedUser);
        workoutPlanRepository.save(workoutPlan);

        //when
        List<WorkoutPlanDto> workoutPlansWhenUserIsCoach = workoutPlanService.getWorkoutPlan();

        //then
        assertThat(workoutPlansWhenUserIsCoach).hasSize(1);
    }

    @Test
    @Transactional
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetWorkoutPlanWhenLoggedUserIsClient() {
        //given
        UserAccount loggedUser = fixture.createClient();
        userAccountRepository.save(loggedUser);

        UserAccount coach = fixture.createCoach();
        userAccountRepository.save(coach);

        WorkoutPlan workoutPlan = fixture.preparePlan(coach);
        workoutPlan.getClients().add(loggedUser);
        workoutPlanRepository.save(workoutPlan);

        //when
        List<WorkoutPlanDto> workoutPlanListForLoggedUserAsClient = workoutPlanService.getWorkoutPlan();

        //then
        assertThat(workoutPlanListForLoggedUserAsClient).hasSize(1);
    }


    @Test
    @Transactional
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldSaveWorkoutPlan() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        WorkoutPlan workoutPlan = fixture.preparePlan(loggedUser);

        //when
        workoutPlanService.saveWorkoutPlan(WorkoutPlanDto.of(workoutPlan));

        //then
        assertThat(workoutPlanRepository.findByCoach(loggedUser)).hasSize(1);
    }

    @Test
    @Transactional
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldUpdateWorkoutPlan() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        WorkoutPlan workoutPlan = fixture.preparePlan(loggedUser);
        workoutPlanRepository.save(workoutPlan);

        WorkoutPlanDto fromForm = WorkoutPlanDto.builder()
                .id(workoutPlan.getId())
                .coach(UserAccountDto.of(loggedUser))
                .clients(new ArrayList<>())
                .status(Status.COMPLETED)
                .workouts(new ArrayList<>())
                .dateCreated(LocalDateTime.now())
                .build();

        //when
        workoutPlanService.updateWorkoutPlan(fromForm);

        //then
        assertThat(workoutPlan.getStatus()).isEqualTo(fromForm.getStatus());
    }

    @Test
    @WithMockUser(Fixture.COACH_EMAIL)
    void shouldRemoveWorkoutPlan() {
        //given
        final UserAccount loggedUser = fixture.createCoach();
        userAccountRepository.save(loggedUser);
        WorkoutPlan workoutPlan = workoutPlanRepository.save(fixture.preparePlan(loggedUser));

        final List<WorkoutPlan> workoutPlans = workoutPlanRepository.findByCoach(loggedUser);

        //when
        workoutPlanService.removeWorkoutPlan(workoutPlan.getId());

        //then
        List<WorkoutPlan> workoutPlansAfterDeletion = workoutPlanRepository.findByCoach(loggedUser);
        assertThat(workoutPlansAfterDeletion).hasSize(workoutPlans.size() - 1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldFailWhenLoggedUserRemovesPlanBeingAClient() {
        //given
        final UserAccount loggedUser = fixture.createClient();
        userAccountRepository.save(loggedUser);
        final UserAccount coach = fixture.createCoach();
        userAccountRepository.save(coach);
        WorkoutPlan workoutPlanToRemove = fixture.preparePlan(coach);

        List<UserAccount> clients = new ArrayList<>();
        clients.add(loggedUser);

        workoutPlanToRemove.setClients(clients);
        workoutPlanRepository.save(workoutPlanToRemove);

        //when, then
        assertThatThrownBy(() -> workoutPlanService.removeWorkoutPlan(workoutPlanToRemove.getId()))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldFailWhenAnotherCoachRemovesWorkoutPlan() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final UserAccount coach = fixture.prepareAccount("secondCoach@Gmail.com", AccountRole.COACH);
        userAccountRepository.save(coach);
        WorkoutPlan workoutPlanToRemove = workoutPlanRepository.save(fixture.preparePlan(coach));

        //when, then
        assertThatThrownBy(() -> workoutPlanService.removeWorkoutPlan(workoutPlanToRemove.getId()))
                .isInstanceOf(RuntimeException.class);

    }
}