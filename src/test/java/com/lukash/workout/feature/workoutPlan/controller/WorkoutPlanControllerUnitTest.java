package com.lukash.workout.feature.workoutPlan.controller;

import com.lukash.workout.feature.workoutPlan.dto.WorkoutPlanDto;
import com.lukash.workout.feature.workoutPlan.model.WorkoutPlan;
import com.lukash.workout.feature.workoutPlan.service.WorkoutPlanService;
import com.lukash.workout.setup.Fixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WorkoutPlanControllerUnitTest {

    final Fixture fixture = new Fixture();

    @Mock
    WorkoutPlanService workoutPlanService;
    @InjectMocks
    WorkoutPlanController workoutPlanController;

    @Test
    void shouldGetWorkoutPlans() {
        //given
        final WorkoutPlanDto workoutPlanDto = WorkoutPlanDto.of(createMockPlan());

        when(workoutPlanService.getWorkoutPlan()).thenReturn(List.of(workoutPlanDto));

        //when
        final List<WorkoutPlanDto> workoutPlanDtoList = workoutPlanController.get();

        //then
        verify(workoutPlanService, times(1)).getWorkoutPlan();
        assertThat(workoutPlanDtoList).hasSize(1)
                .extracting((id) -> workoutPlanDto.getId())
                .containsOnly(workoutPlanDto.getId());
    }

    @Test
    void create() {
        //given
        final WorkoutPlanDto workoutPlanDtoInput = WorkoutPlanDto.of(createMockPlan());

        //when
        workoutPlanController.create(workoutPlanDtoInput);

        //then
        verify(workoutPlanService, times(1)).saveWorkoutPlan(workoutPlanDtoInput);
    }

    @Test
    void update() {
        //given
        final WorkoutPlanDto workoutPlanDto = WorkoutPlanDto.of(createMockPlan());

        //when
        workoutPlanController.update(workoutPlanDto);

        //then
        verify(workoutPlanService, times(1)).updateWorkoutPlan(workoutPlanDto);
    }

    @Test
    void updateStatus() {
        //given
        final WorkoutPlanDto mockDto = WorkoutPlanDto.of(createMockPlan());

        //when
        workoutPlanController.updateStatus(mockDto.getId(), mockDto.getStatus());

        //then
        verify(workoutPlanService, times(1)).updateStatus(mockDto.getId(), mockDto.getStatus());
    }

    @Test
    void delete() {
        //given
        final WorkoutPlanDto mockDtoToDelete = WorkoutPlanDto.of(createMockPlan());

        //when
        workoutPlanController.delete(mockDtoToDelete.getId());

        //then
        verify(workoutPlanService, times(1)).removeWorkoutPlan(mockDtoToDelete.getId());
    }

    private WorkoutPlan createMockPlan() {
        return fixture.preparePlan(fixture.createCoach());
    }
}