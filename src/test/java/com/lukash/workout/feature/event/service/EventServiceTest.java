package com.lukash.workout.feature.event.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.event.model.Event;
import com.lukash.workout.feature.event.repository.EventRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class EventServiceTest extends IntegrationTest {

    @Autowired
    UserAccountRepository userAccountRepository;
    @Autowired
    EventRepository eventRepository;
    @Autowired
    EventService eventService;

    Fixture fixture = new Fixture();

    @AfterEach
    void cleanUp() {
        eventRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @Test
    @Transactional
    @WithMockUser(Fixture.USER_EMAIL)
    void fetchByLoggedUserAsACreator() {
        //given

        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);

        int numberOfEvents = 4;
        final List<Event> eventsOfLoggedUserAsCreator = fixture.prepareMultipleEvents(loggedUser,numberOfEvents);
        eventRepository.saveAll(eventsOfLoggedUserAsCreator);

        //when
        List<Event> allEventsOfLoggedUser = eventService.fetchForUser();
        //then
        assertThat(allEventsOfLoggedUser).hasSize(numberOfEvents);
    }

    @Test
    @Transactional
    @WithMockUser(Fixture.USER_EMAIL)
    void fetchByLoggedUserAsAParticipant(){
        //given
        int numberOfEvents = 4;
        int numberOfAccounts = 5;

        final UserAccount testUser = fixture.prepareAccount("testuser@Gmail.com", AccountRole.CLIENT);
        userAccountRepository.save(testUser);

        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(testUser);

        final List<UserAccount> participants = fixture.prepareAccounts(numberOfAccounts);
        participants.add(loggedUser);
        userAccountRepository.saveAll(participants);

        final List<Event> eventsOfTestUser = fixture.prepareMultipleEvents(testUser, numberOfEvents);
        eventRepository.saveAll(eventsOfTestUser);

        Event testUserEvent = eventsOfTestUser.get(0);
        testUserEvent.setParticipants(participants);

        //when
        List<Event> eventsOfLoggedUserAsParticipant = eventService.fetchForUser();

        //then
        assertThat(eventsOfLoggedUserAsParticipant).hasSize(1);
    }

    @Test
    @Transactional
    @WithMockUser(Fixture.USER_EMAIL)
    void createEvent() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final List<Event> eventsBeforeCreation = eventRepository.findByCreatedBy(loggedUser);

        final Event eventToCreate = fixture.prepareEvent(loggedUser);
        //when
        eventService.createEvent(eventToCreate);
        //then
        final List<Event> eventsAfterCreation = eventRepository.findByCreatedBy(loggedUser);
        assertThat(eventsAfterCreation).hasSize(eventsBeforeCreation.size() + 1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void updateEvent() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final Event eventBeforeUpdate = fixture.prepareEvent(loggedUser);
        eventRepository.save(eventBeforeUpdate);

        final Event eventFromForm = Event.builder()
                .id(eventBeforeUpdate.getId())
                .title("UpdatedTitle")
                .createdBy(eventBeforeUpdate.getCreatedBy())
                .start(OffsetDateTime.now().minusHours(2))
                .end(OffsetDateTime.now().minusHours(2))
                .description("Updated content")
                .build();
        //when
        eventService.updateEvent(eventFromForm);
        //then
        Event eventAfterUpdate = eventRepository.findById(eventBeforeUpdate.getId()).get();

        assertThat(eventAfterUpdate.getDescription()).isNotEqualTo(eventBeforeUpdate.getDescription());
        assertThat(eventAfterUpdate.getStart()).isNotEqualTo(eventBeforeUpdate.getStart());
        assertThat(eventAfterUpdate.getEnd()).isNotEqualTo(eventBeforeUpdate.getStart());
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    @Transactional
    void deleteEvent() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        final int numberOfEvents = 3;
        final List<Event> eventListBeforeRemoval = fixture.prepareMultipleEvents(loggedUser, numberOfEvents);
        userAccountRepository.save(loggedUser);
        eventRepository.saveAll(eventListBeforeRemoval);
        //when
        eventService.deleteEvent(eventListBeforeRemoval.get(0).getId());
        //
        final List<Event> eventListAfterRemoval = eventRepository.findByCreatedBy(loggedUser);
        assertThat(eventListAfterRemoval).hasSize(eventListBeforeRemoval.size() - 1);
    }
}