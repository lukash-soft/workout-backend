package com.lukash.workout.feature.event.controller;

import com.lukash.workout.feature.event.model.Event;
import com.lukash.workout.feature.event.service.EventService;
import com.lukash.workout.setup.Fixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EventControllerUnitTest {

    final Fixture fixture = new Fixture();

    @Mock
    EventService eventService;
    @InjectMocks
    EventController eventController;

    @Test
    void fetchForUser() {
        //given, when
        eventController.fetchForUser();
        //then
        verify(eventService, times(1)).fetchForUser();
    }

    @Test
    void createEvent() {
        //given
        final Event eventToCreate = createMockEvent();

        //when
        eventController.createEvent(eventToCreate);

        //then
        verify(eventService,times(1)).createEvent(eventToCreate);

    }

    @Test
    void updateEvent() {
        //given

        final Event eventToUpdate = createMockEvent();
        eventToUpdate.setId(1L);

        when(eventService.updateEvent(eventToUpdate)).thenReturn(eventToUpdate);

        //when
        eventController.updateEvent(eventToUpdate);

        //then
        verify(eventService,times(1)).updateEvent(eventToUpdate);
    }

    @Test
    void deleteEvent() {
        //given
        final Event eventToDelete = createMockEvent();

        //when
        eventController.deleteEvent(eventToDelete.getId());

        //then
        verify(eventService,times(1)).deleteEvent(eventToDelete.getId());
    }

    private Event createMockEvent() {
        return fixture.prepareEvent(fixture.createCoach());
    }
}