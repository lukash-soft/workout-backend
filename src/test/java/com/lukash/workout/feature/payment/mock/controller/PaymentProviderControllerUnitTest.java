package com.lukash.workout.feature.payment.mock.controller;

import com.lukash.workout.feature.payment.dto.PaymentDto;
import com.lukash.workout.feature.payment.mock.service.PaymentProviderService;
import com.lukash.workout.setup.Fixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class PaymentProviderControllerUnitTest {

    final Fixture fixture = new Fixture();

    @Mock
    PaymentProviderService providerService;
    @InjectMocks
    PaymentProviderController paymentProviderController;

    @Test
    void shouldSavePayment() {
        //given
        final PaymentDto paymentDto = createMockPayment();
        //when
        paymentProviderController.savePayment(paymentDto);
        //then
        verify(providerService, times(1)).savePayment(paymentDto);
    }

    private PaymentDto createMockPayment() {
        return PaymentDto.of(fixture.preparePayment(fixture.createClient()));
    }
}