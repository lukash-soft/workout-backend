package com.lukash.workout.feature.payment.invoicing.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.payment.invoicing.dto.InvoiceDto;
import com.lukash.workout.feature.payment.invoicing.model.Invoice;
import com.lukash.workout.feature.payment.invoicing.repository.InvoiceRepository;
import com.lukash.workout.feature.payment.mock.repository.PaymentProviderRepository;
import com.lukash.workout.feature.subscription.model.Subscription;
import com.lukash.workout.feature.subscription.repository.SubscriptionRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class InvoiceServiceTest extends IntegrationTest {

    @Autowired
    InvoiceRepository invoiceRepository;
    @Autowired
    UserAccountRepository userAccountRepository;
    @Autowired
    InvoiceService invoiceService;
    @Autowired
    PaymentProviderRepository paymentProviderRepository;
    @Autowired
    SubscriptionRepository subscriptionRepository;

    final static Fixture fixture = new Fixture();

    @AfterEach
    void cleanUp() {
        invoiceRepository.deleteAll();
        subscriptionRepository.deleteAll();
        paymentProviderRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetForUser() {
        //given
        final UserAccount user = fixture.createClient();
        userAccountRepository.save(user);
        Invoice invoice = fixture.prepareInvoice(user);
        invoiceRepository.save(invoice);

        //when
        List<InvoiceDto> invoiceDto = invoiceService.getForUser(LocalDate.now().minusDays(1));

        //then
        assertThat(invoiceDto).hasSize(1);
    }

    @ParameterizedTest
    @MethodSource("provideMultipleCoaches")
    void shouldCreateInvoice(List<UserAccount> coaches) {
        //given
        final UserAccount client = fixture.createClient();
        userAccountRepository.save(client);

        final List<UserAccount> availableCoaches = userAccountRepository.saveAll(coaches);
        List<Subscription> subscriptions = new ArrayList<>();
        double totalInvoiceAmount = 0.0;

        for (UserAccount coach : availableCoaches) {
            Subscription subscription = fixture.prepareSubscription(coach, client);
            subscriptions.add(subscription);
            totalInvoiceAmount += subscription.getFee();
            subscriptionRepository.save(subscription);
        }

        //when
        invoiceService.create();
        //then
        List<Invoice> invoices = invoiceRepository.findByCreatedDayAfterAndUser(LocalDate.now().minusDays(3), client);
        Invoice invoice = invoices.get(0);

        List<UserAccount> clients = userAccountRepository.findByAccountRole(AccountRole.CLIENT);

        assertThat(invoices).hasSize(clients.size());
        assertThat(invoice.getTotalAmount()).isEqualTo(totalInvoiceAmount);
    }

    @Test
    void shouldNotCreateDuplicateInvoice() {
        //given
        final UserAccount client = fixture.createClient();
        userAccountRepository.save(client);
        final UserAccount coach = fixture.createCoach();
        userAccountRepository.save(coach);
        final Subscription subscription = fixture.prepareSubscription(coach, client);
        subscriptionRepository.save(subscription);
        double totalInvoiceAmount = subscription.getFee();

        Invoice invoice = Invoice.builder()
                .user(client)
                .createdDay(LocalDate.now())
                .totalAmount(totalInvoiceAmount)
                .build();
        invoiceRepository.save(invoice);
        List<Invoice> invoices = invoiceRepository.findByCreatedDayAfterAndUser(LocalDate.now().minusDays(3), client);

        //when
        invoiceService.create();

        //then
        assertThat(invoices.size()).isEqualTo(1);
    }

    static List<List<UserAccount>> provideMultipleCoaches() {
        final UserAccount coachOne =
                fixture.prepareAccount("coachOne@gmail", AccountRole.COACH);
        final UserAccount coachTwo =
                fixture.prepareAccount("coachTwo@gmail", AccountRole.COACH);
        final UserAccount coachThree =
                fixture.prepareAccount("coachThree@gmail", AccountRole.COACH);

        return List.of(
                List.of(coachOne),
                List.of(coachOne, coachTwo),
                List.of(coachOne, coachTwo, coachThree)
        );
    }
}