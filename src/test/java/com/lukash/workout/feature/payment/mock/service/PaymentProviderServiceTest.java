package com.lukash.workout.feature.payment.mock.service;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.payment.dto.PaymentDto;
import com.lukash.workout.feature.payment.invoicing.model.Invoice;
import com.lukash.workout.feature.payment.invoicing.repository.InvoiceRepository;
import com.lukash.workout.feature.payment.mock.exception.CardException;
import com.lukash.workout.feature.payment.mock.repository.PaymentProviderRepository;
import com.lukash.workout.feature.payment.model.Payment;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

class PaymentProviderServiceTest extends IntegrationTest {

    @Autowired
    PaymentProviderRepository paymentProviderRepository;
    @Autowired
    UserAccountRepository userAccountRepository;
    @Autowired
    PaymentProviderService paymentProviderService;
    @Autowired
    InvoiceRepository invoiceRepository;

    final Fixture fixture = new Fixture();

    @AfterEach
    void cleanUp() {
        invoiceRepository.deleteAll();
        paymentProviderRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldSavePaymentIfLastFourDigitsAreCorrect() {
        //given
        final UserAccount loggedUser = fixture.createClient();
        userAccountRepository.save(loggedUser);
        final Invoice invoice = fixture.prepareInvoice(loggedUser);
        invoiceRepository.save(invoice);

        PaymentDto paymentDto = PaymentDto.builder()
                .amountPaid(invoice.getTotalAmount())
                .cardNumber("1234123412341234")
                .expirationDate(LocalDate.now().plusDays(1))
                .securityCode("123")
                .invoiceId(invoice.getId())
                .build();

        //when
        paymentProviderService.savePayment(paymentDto);

        //then
        List<Payment> payments = paymentProviderRepository.findAll();
        Payment paymentSaved = paymentProviderRepository.findByUser(loggedUser)
                .orElseThrow();
        assertThat(payments).hasSize(1);
        assertThat(paymentSaved.getCardNumber().length()).isEqualTo(4);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldThrowErrorWhenCardNumbersAreTooFew() {
        //given
        final UserAccount loggedUser = fixture.createClient();
        userAccountRepository.save(loggedUser);
        final Invoice invoice = fixture.prepareInvoice(loggedUser);
        invoiceRepository.save(invoice);


        PaymentDto paymentDto = PaymentDto.builder()
                .amountPaid(invoice.getTotalAmount())
                .cardNumber("123412341234")
                .expirationDate(LocalDate.now().plusDays(1))
                .securityCode("124")
                .invoiceId(invoice.getId())
                .build();

        //when, then
        assertThatThrownBy(() -> paymentProviderService.savePayment(paymentDto)).isInstanceOf(CardException.class);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldThrowErrorWhenSecurityCodeIsTooShort() {
        //given
        final UserAccount loggedUser = fixture.createClient();
        userAccountRepository.save(loggedUser);
        final Invoice invoice = fixture.prepareInvoice(loggedUser);
        invoiceRepository.save(invoice);

        PaymentDto paymentDto = PaymentDto.builder()
                .amountPaid(invoice.getTotalAmount())
                .cardNumber("1234123412341234")
                .expirationDate(LocalDate.now().plusDays(1))
                .securityCode("14")
                .invoiceId(invoice.getId())
                .build();

        //when, then
        assertThatThrownBy(() -> paymentProviderService.savePayment(paymentDto)).isInstanceOf(CardException.class);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldThrowErrorWhenExpirationDateIsDue() {
        //given
        final UserAccount loggedUser = fixture.createClient();
        userAccountRepository.save(loggedUser);
        final Invoice invoice = fixture.prepareInvoice(loggedUser);
        invoiceRepository.save(invoice);

        PaymentDto paymentDto = PaymentDto.builder()
                .amountPaid(invoice.getTotalAmount())
                .cardNumber("1234123412341234")
                .expirationDate(LocalDate.now().minusDays(1))
                .securityCode("124")
                .invoiceId(invoice.getId())
                .build();

        //when, then
        assertThatThrownBy(() -> paymentProviderService.savePayment(paymentDto)).isInstanceOf(CardException.class);
    }
}