package com.lukash.workout.feature.athlete_details.controller;

import com.lukash.workout.feature.athlete_details.model.AthleteDetails;
import com.lukash.workout.feature.athlete_details.service.AthleteDetailsService;
import com.lukash.workout.setup.Fixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AthleteDetailsControllerUnitTest {

    final Fixture fixture = new Fixture();

    @Mock
    AthleteDetailsService athleteDetailsService;
    @InjectMocks
    AthleteDetailsController athleteDetailsController;

    @Test
    void addAthleteDetails(){
        //given
        final AthleteDetails athleteDetails = createMockDetails();
        //when
        athleteDetailsController.addAthleteDetails(athleteDetails);
        //then
        verify(athleteDetailsService, times(1)).addAthleteDetails(athleteDetails);
    }

    @Test
    void updateAthleteDetails() {
        //given
        final AthleteDetails athleteDetails = createMockDetails();

        //when
        athleteDetailsController.updateAthleteDetails(athleteDetails);

        //then
        verify(athleteDetailsService, times(1)).updateAthleteDetails(athleteDetails);
    }

    @Test
    void getAthleteDetailsForUser() {
        //given, when
        athleteDetailsController.getAthleteDetailsForUser();

        //then
        verify(athleteDetailsService, times(1)).getAthleteDetailsForUser();
    }

    private AthleteDetails createMockDetails() {
        return fixture.prepareAthleteDetails(fixture.createClient());
    }

}