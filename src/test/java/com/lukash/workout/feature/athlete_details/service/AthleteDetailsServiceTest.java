package com.lukash.workout.feature.athlete_details.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.athlete_details.model.AthleteDetails;
import com.lukash.workout.feature.athlete_details.repository.AthleteDetailsRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


class AthleteDetailsServiceTest extends IntegrationTest {

    @Autowired
    AthleteDetailsService athleteDetailsService;

    @Autowired
    AthleteDetailsRepository athleteDetailsRepository;

    @Autowired
    UserAccountRepository userAccountRepository;


    Fixture fixture = new Fixture();

    //this method will be run after each test
    @AfterEach
    void cleanup() {
        athleteDetailsRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetAthleteDetailsForUser() {
        //given
        UserAccount loggedUser = fixture.prepareAccount(fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);

        AthleteDetails athleteDetailsToGet = AthleteDetails.builder()
                .height(175)
                .squat(150)
                .waistMeasurement(88.0)
                .armMeasurement(37.0)
                .thighMeasurement(90.0)
                .weight(120)
                .userAccount(loggedUser)
                .benchPress(120)
                .build();

        athleteDetailsRepository.save(athleteDetailsToGet);

        //when
        final List<AthleteDetails> result = athleteDetailsService.getAthleteDetailsForUser();

        //then
        assertThat(result).isNotEmpty();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldAddAthleteDetails() {
        //given
        UserAccount loggedUser = fixture.createClient();
        userAccountRepository.save(loggedUser);
        AthleteDetails athleteDetailsToAdd = AthleteDetails.builder()
                .height(175)
                .squat(150)
                .waistMeasurement(88.0)
                .armMeasurement(37.0)
                .thighMeasurement(90.0)
                .weight(120)
                .userAccount(loggedUser)
                .benchPress(120)
                .build();
        //when
        final AthleteDetails athleteDetailsAdded = athleteDetailsService.addAthleteDetails(athleteDetailsToAdd);
        //then
        assertThat(athleteDetailsToAdd.getSquat()).isEqualTo(athleteDetailsAdded.getSquat());
        assertThat(athleteDetailsToAdd.getHeight()).isEqualTo(athleteDetailsAdded.getHeight());
        assertThat(athleteDetailsToAdd.getWeight()).isEqualTo(athleteDetailsAdded.getWeight());
        assertThat(athleteDetailsToAdd.getBenchPress()).isEqualTo(athleteDetailsAdded.getBenchPress());
        assertThat(athleteDetailsToAdd.getArmMeasurement()).isEqualTo(athleteDetailsAdded.getArmMeasurement());
        assertThat(athleteDetailsToAdd.getWaistMeasurement()).isEqualTo(athleteDetailsAdded.getWaistMeasurement());
        assertThat(athleteDetailsToAdd.getThighMeasurement()).isEqualTo(athleteDetailsAdded.getThighMeasurement());
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void updateAthleteDetails() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(fixture.USER_EMAIL, AccountRole.CLIENT);
        userAccountRepository.save(loggedUser);

        final AthleteDetails athleteDetailsBeforeUpdate = AthleteDetails.builder()
                .benchPress(100)
                .height(175)
                .squat(140)
                .waistMeasurement(90.0)
                .armMeasurement(40.0)
                .thighMeasurement(99.0)
                .weight(85)
                .userAccount(loggedUser)
                .build();
        athleteDetailsRepository.save(athleteDetailsBeforeUpdate);

        final AthleteDetails fromForm = AthleteDetails.builder()
                .benchPress(80)
                .height(172)
                .squat(122)
                .waistMeasurement(100.0)
                .armMeasurement(45.0)
                .thighMeasurement(110.0)
                .weight(80)
                .userAccount(loggedUser)
                .build();
        athleteDetailsRepository.save(fromForm);

        //when
        final AthleteDetails athleteDetailsAfterUpdate = athleteDetailsService.updateAthleteDetails(fromForm);

        //then
        assertThat(athleteDetailsAfterUpdate.getThighMeasurement()).isEqualTo(fromForm.getThighMeasurement());
        assertThat(athleteDetailsAfterUpdate.getHeight()).isEqualTo(fromForm.getHeight());
        assertThat(athleteDetailsAfterUpdate.getWeight()).isEqualTo(fromForm.getWeight());
        assertThat(athleteDetailsAfterUpdate.getBenchPress()).isEqualTo(fromForm.getBenchPress());
        assertThat(athleteDetailsAfterUpdate.getSquat()).isEqualTo(fromForm.getSquat());
        assertThat(athleteDetailsAfterUpdate.getWaistMeasurement()).isEqualTo(fromForm.getWaistMeasurement());
        assertThat(athleteDetailsAfterUpdate.getArmMeasurement()).isEqualTo(fromForm.getArmMeasurement());


    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldDeleteAthleteDetails() {
        //given
        final UserAccount loggedUser = fixture.createClient();
        userAccountRepository.save(loggedUser);
        final AthleteDetails athleteDetails = fixture.prepareAthleteDetails(loggedUser);
        athleteDetailsRepository.save(athleteDetails);

        final List<AthleteDetails> athleteDetailsListBeforeDeletion = athleteDetailsRepository.findByUserAccount(loggedUser);

        //when
        athleteDetailsService.deleteAthleteDetails(athleteDetails.getId());

        //then
        final List<AthleteDetails> athleteDetailsAfterDeletion = athleteDetailsRepository.findByUserAccount(loggedUser);
        assertThat(athleteDetailsListBeforeDeletion.size()).isEqualTo(1);
        assertThat(athleteDetailsAfterDeletion).isEmpty();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldCheckIfBenchPressIsZero() {
        //given, when
        final UserAccount loggedUser = fixture.prepareAccount(fixture.USER_EMAIL, AccountRole.CLIENT);
        AthleteDetails athleteDetailsToTest = AthleteDetails.builder()
                .weight(60.0)
                .benchPress(0)
                .squat(100)
                .height(150.0)
                .userAccount(loggedUser)
                .build();
        userAccountRepository.save(loggedUser);
        athleteDetailsRepository.save(athleteDetailsToTest);

        final AthleteDetails result = athleteDetailsRepository.findByUserAccountId(loggedUser.getId()).get();
        //then
        assertThat(result.getBenchPress()).isEqualTo(0);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldCheckIfSquatIsZero() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(fixture.USER_EMAIL, AccountRole.CLIENT);
        AthleteDetails athleteDetailsToTest = AthleteDetails.builder()
                .weight(60.0)
                .benchPress(120)
                .squat(0)
                .height(150.0)
                .userAccount(loggedUser)
                .build();
        userAccountRepository.save(loggedUser);
        athleteDetailsRepository.save(athleteDetailsToTest);
        //when
        final AthleteDetails result = athleteDetailsRepository.findByUserAccountId(loggedUser.getId()).get();
        //then
        assertThat(result.getSquat()).isEqualTo(0);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldCheckIfWeightIsZero() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(fixture.USER_EMAIL, AccountRole.CLIENT);
        AthleteDetails athleteDetailsToTest = AthleteDetails.builder()
                .weight(0)
                .benchPress(110)
                .squat(100)
                .height(150.0)
                .userAccount(loggedUser)
                .build();
        userAccountRepository.save(loggedUser);
        athleteDetailsRepository.save(athleteDetailsToTest);
        //when
        final AthleteDetails result = athleteDetailsRepository.findByUserAccountId(loggedUser.getId()).get();
        //then
        assertThat(result.getWeight()).isLessThan(1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldCheckIfHeightIsZero() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        AthleteDetails athleteDetailsToTest = AthleteDetails.builder()
                .weight(75.0)
                .benchPress(110)
                .squat(100)
                .height(0)
                .userAccount(loggedUser)
                .build();
        userAccountRepository.save(loggedUser);
        athleteDetailsRepository.save(athleteDetailsToTest);
        //when
        final AthleteDetails result = athleteDetailsRepository.findByUserAccountId(loggedUser.getId()).get();
        //then
        assertThat(result.getHeight()).isEqualTo(0);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldCheckIfWaistMeasurementIsZero() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        AthleteDetails athleteDetailsToTest = AthleteDetails.builder()
                .weight(75.0)
                .benchPress(110)
                .waistMeasurement(0.0)
                .squat(100)
                .height(123)
                .userAccount(loggedUser)
                .build();
        userAccountRepository.save(loggedUser);
        athleteDetailsRepository.save(athleteDetailsToTest);
        //when
        final AthleteDetails result = athleteDetailsRepository.findByUserAccountId(loggedUser.getId()).get();
        //then
        assertThat(result.getWaistMeasurement()).isEqualTo(0);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldCheckIfArmMeasurementIsZero() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        AthleteDetails athleteDetailsToTest = AthleteDetails.builder()
                .weight(75.0)
                .benchPress(110)
                .squat(100)
                .armMeasurement(0.0)
                .height(165)
                .userAccount(loggedUser)
                .build();
        userAccountRepository.save(loggedUser);
        athleteDetailsRepository.save(athleteDetailsToTest);
        //when
        final AthleteDetails result = athleteDetailsRepository.findByUserAccountId(loggedUser.getId()).get();
        //then
        assertThat(result.getArmMeasurement()).isEqualTo(0);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldCheckIfThighMeasurementIsZero() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        AthleteDetails athleteDetailsToTest = AthleteDetails.builder()
                .weight(75.0)
                .benchPress(110)
                .squat(100)
                .height(123)
                .thighMeasurement(0.0)
                .userAccount(loggedUser)
                .build();
        userAccountRepository.save(loggedUser);
        athleteDetailsRepository.save(athleteDetailsToTest);
        //when
        final AthleteDetails result = athleteDetailsRepository.findByUserAccountId(loggedUser.getId()).get();
        //then
        assertThat(result.getThighMeasurement()).isEqualTo(0);
    }

}
