package com.lukash.workout.feature.picutre.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.picutre.model.Picture;
import com.lukash.workout.feature.picutre.repository.PictureRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class PictureServiceTest extends IntegrationTest {

    @Autowired
    PictureService pictureService;

    @Autowired
    PictureRepository pictureRepository;

    @Autowired
    UserAccountRepository userAccountRepository;

    Fixture fixture = new Fixture();

    @AfterEach
    void cleanup() {
        pictureRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @AfterEach
    void cleanUp() throws IOException {
        FileUtils.deleteDirectory(new File(PictureService.IMAGE_FOLDER_DIR));
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetForUser() {
        //given
        UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        Picture testPicture = fixture.preparePicture(loggedUser);
        userAccountRepository.save(loggedUser);
        pictureRepository.save(testPicture);
        //when
        byte[] imageByteArray = pictureService.getForUser(testPicture.getId());
        //then
        assertThat(imageByteArray).isNotEmpty();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldSavePicture() throws IOException {
        //given
        final String filename = "image.png";
        final MultipartFile multipartFile = createMultipartFile(filename);
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        //when
        pictureService.savePicture(multipartFile);
        //then;
        List<Picture> pictureListFromDB = pictureRepository.getByAddedBy_Id(loggedUser.getId());
        Picture picture = pictureListFromDB.get(0);
        assertThat(new File(picture.getPath()).isFile()).isEqualTo(true);
    }

    private MultipartFile createMultipartFile(final String filename) throws IOException {
        final Resource resource = new ClassPathResource("images/image.png");
        final InputStream input = resource.getInputStream();
        return new MockMultipartFile(filename, filename, null, input);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    @Transactional
    void shouldTestSwitchingProfilePicture() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final Picture oldProfilePicture = fixture.preparePicture(loggedUser);
        oldProfilePicture.setProfile(true);
        pictureRepository.save(oldProfilePicture);


        final Picture newProfilePicture = fixture.preparePicture(loggedUser);
        pictureRepository.save(newProfilePicture);
        assertThat(newProfilePicture.isProfile()).isFalse();

        //when
        pictureService.switchProfilePicture(newProfilePicture.getId());
        //then
        final Picture oldPictureAfterUpdate = pictureRepository.findById(oldProfilePicture.getId()).get();
        final Picture newPictureAfterUpdate = pictureRepository.findById(newProfilePicture.getId()).get();

        assertThat(newPictureAfterUpdate.isProfile()).isTrue();
        assertThat(oldPictureAfterUpdate.isProfile()).isFalse();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldSaveProfilePicWhenUserNeverHadOne() throws IOException {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final String profilePicName = "profilePic.png";
        final MultipartFile multipartFile = createMultipartFile(profilePicName);
        //when
        Picture picture = pictureService.saveProfilePicture(multipartFile);
        //then
        assertThat(picture.isProfile()).isTrue();
    }

    @Test
    @Transactional
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldSaveProfilePicWhenUserHadOne() throws IOException {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);

        final String oldPicName = "profilePic.png";
        final MultipartFile oldProfilePic = createMultipartFile(oldPicName);
        Picture oldProfilePicture = pictureService.saveProfilePicture(oldProfilePic);
        pictureRepository.save(oldProfilePicture);


        final String newPicName = "newProfilePic.png";
        final MultipartFile profilePic = createMultipartFile(newPicName);
        //when
        Picture newProfilePicture = pictureService.saveProfilePicture(profilePic);
        //then
        assertThat(oldProfilePicture.isProfile()).isFalse();
        assertThat(newProfilePicture.isProfile()).isTrue();
    }


    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void removePicture() throws IOException {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        Path sourceFile = Paths.get(Fixture.TEST_IMAGE_DIR + "/image.png");
        Path targetPath = Paths.get(Fixture.TEST_IMAGE_DIR + "/copyImage.png");
        Files.copy(sourceFile, targetPath, StandardCopyOption.COPY_ATTRIBUTES);

        Picture pictureToRemove = Picture.builder()
                .path(targetPath.toString())
                .addedBy(loggedUser)
                .build();
        pictureRepository.save(pictureToRemove);

        int picturesBeforeRemoval = pictureRepository.getByAddedBy_Id(loggedUser.getId()).size();
        //then
        pictureService.removePicture(pictureToRemove.getId());
        //when
        int picturesAfterRemoval = pictureRepository.getByAddedBy_Id(loggedUser.getId()).size();
        assertThat(picturesAfterRemoval).isEqualTo(picturesBeforeRemoval - 1);

        Optional<Picture> removedPicture = pictureRepository.getByAddedBy_IdAndId(loggedUser.getId(), pictureToRemove.getId());
        assertThat(removedPicture).isEmpty();
    }

    @Test
    void shouldThrowExceptionIfPictureIsNull() {
        //given, when, then
        assertThatThrownBy(() -> pictureService.savePicture(null)).isInstanceOf(RuntimeException.class);
    }


    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void sendUserPicturesId() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final List<Picture> picturesOfLoggedUser = fixture.preparePictures(loggedUser, 3);
        pictureRepository.saveAll(picturesOfLoggedUser);
        //when
        final List<Long> picturesIDs = pictureService.getPicturesIdsForLoggedUser();
        //then
        assertThat(picturesIDs).isNotEmpty();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void getProfilePictureForUserWhenItHasOne() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final Picture profilePic = fixture.preparePicture(loggedUser);
        profilePic.setProfile(true);
        pictureRepository.save(profilePic);
        //when
        byte[] profilePicByteArray = pictureService.getProfilePictureForUser();
        //then
        assertThat(profilePicByteArray).isNotEmpty();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void getProfilePictureForUserWhenItDoesntHaveOne() {
        //given,
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        // when, then
        assertThatThrownBy(() -> pictureService.getProfilePictureForUser()).isInstanceOf(ResponseStatusException.class);
    }
}
