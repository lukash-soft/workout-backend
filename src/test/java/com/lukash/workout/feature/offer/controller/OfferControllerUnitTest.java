package com.lukash.workout.feature.offer.controller;

import com.lukash.workout.feature.offer.dto.OfferDto;
import com.lukash.workout.feature.offer.model.OfferModel;
import com.lukash.workout.feature.offer.service.OfferService;
import com.lukash.workout.setup.Fixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OfferControllerUnitTest {

    final Fixture fixture = new Fixture();

    @Mock
    OfferService offerService;
    @InjectMocks
    OfferController offerController;

    @Test
    void get() {
        //given
        final OfferDto offerDto = OfferDto.of(createMockOfferDto());
        when(offerController.getOffers()).thenReturn(List.of(offerDto));

        //when
        offerController.getOffers();

        //then
        verify(offerService, times(1)).getOffers();
    }



    private OfferModel createMockOfferDto() {
        return fixture.prepareOffer(fixture.createCoach());
    }

    @Test
    void create() {
        //given
        final OfferDto offerDto = OfferDto.of(createMockOfferDto());

        //when
        offerController.create(offerDto);

        //then
        verify(offerService, times(1)).create(offerDto);
    }

    @Test
    void update() {
        //given
        final OfferDto offerDto = OfferDto.of(createMockOfferDto());

        //when
        offerController.update(offerDto);

        //then
        verify(offerService, times(1)).update(offerDto);
    }

    @Test
    void delete() {
        //given
        final OfferDto offerDto = OfferDto.of(createMockOfferDto());

        //when
        offerController.delete(offerDto.getId());

        //then
        verify(offerService, times(1)).delete(offerDto.getId());
    }
}