package com.lukash.workout.feature.offer;

import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.offer.dto.OfferDto;
import com.lukash.workout.feature.offer.exception.PriceException;
import com.lukash.workout.feature.offer.model.OfferModel;
import com.lukash.workout.feature.offer.repository.OfferRepository;
import com.lukash.workout.feature.offer.service.OfferService;
import com.lukash.workout.feature.subscription.model.Subscription;
import com.lukash.workout.feature.subscription.repository.SubscriptionRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class OfferServiceTest extends IntegrationTest {

    @Autowired
    UserAccountRepository userAccountRepository;
    @Autowired
    OfferRepository offerRepository;
    @Autowired
    OfferService offerService;
    @Autowired
    SubscriptionRepository subscriptionRepository;

    final static Fixture fixture = new Fixture();

    @AfterEach
    void cleanUp() {
        offerRepository.deleteAll();
        subscriptionRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetForClient() {
        //given
        final UserAccount client = fixture.createClient();
        userAccountRepository.save(client);
        final UserAccount coach = fixture.createCoach();
        userAccountRepository.save(coach);

        OfferModel offerModel = fixture.prepareOffer(coach);
        offerRepository.save(offerModel);

        Subscription subscription = fixture.prepareSubscription(coach, client);
        subscription.setOfferId(offerModel.getId());
        subscriptionRepository.save(subscription);

        //when
        List<OfferDto> offersForClient = offerService.getOffers();

        //then
        assertThat(offersForClient).isEmpty();
    }


    @Test
    @WithMockUser(Fixture.COACH_EMAIL)
    void shouldGetForCoach() {
        //given
        final UserAccount coach = fixture.createCoach();
        userAccountRepository.save(coach);
        OfferModel offerModel = fixture.prepareOffer(coach);
        offerRepository.save(offerModel);

        //when
        List<OfferDto> offerToGet = offerService.getOffers();

        //then
        assertThat(offerToGet).isNotEmpty();
    }

    @Test
    @WithMockUser(Fixture.COACH_EMAIL)
    void shouldCreate() {
        //given
        final UserAccount coach = fixture.createCoach();
        userAccountRepository.save(coach);

        OfferModel fromForm = OfferModel.builder()
                .coach(coach)
                .title("Created title")
                .description("Create description")
                .price(20.0)
                .build();

        //when
        offerService.create(OfferDto.of(fromForm));

        //then
        assertThat(offerRepository.findAll()).hasSize(1);
    }

    @Test
    @WithMockUser(Fixture.COACH_EMAIL)
    void shouldThrowExceptionWhenCreatingWithNegativePrice() {
        //given
        final UserAccount coach = fixture.createCoach();
        userAccountRepository.save(coach);

        OfferModel fromForm = OfferModel.builder()
                .coach(coach)
                .title("Created title")
                .description("Create description")
                .price(-20.0)
                .build();

        //when, then
        assertThatThrownBy(() -> offerService.create(OfferDto.of(fromForm))).isInstanceOf(PriceException.class);
    }

    @Test
    @WithMockUser(Fixture.COACH_EMAIL)
    void shouldUpdate() {
        //given
        final UserAccount coach = fixture.createCoach();
        userAccountRepository.save(coach);

        OfferModel existingOffer = fixture.prepareOffer(coach);
        offerRepository.save(existingOffer);

        OfferDto offerFromForm = OfferDto.builder()
                .id(existingOffer.getId())
                .coach(null)
                .title("Updated title")
                .description("Updated descr")
                .price(503.0)
                .build();

        //when
        offerService.update(offerFromForm);

        //then
        OfferModel updatedExistingOffer = offerRepository.findById(existingOffer.getId()).get();

        assertThat(updatedExistingOffer.getCoach().getId()).isEqualTo(existingOffer.getCoach().getId());
        assertThat(updatedExistingOffer.getDescription()).isEqualTo(offerFromForm.getDescription());
        assertThat(updatedExistingOffer.getTitle()).isEqualTo(offerFromForm.getTitle());
        assertThat(updatedExistingOffer.getPrice()).isEqualTo(offerFromForm.getPrice());
    }

    @Test
    @WithMockUser(Fixture.COACH_EMAIL)
    void shouldDelete() {
        //given
        final UserAccount coach = fixture.createCoach();
        userAccountRepository.save(coach);

        OfferModel offerToRemove = fixture.prepareOffer(coach);
        offerRepository.save(offerToRemove);

        List<OfferModel> offerListBeforeUpdate = offerRepository.findAll();

        //when
        offerService.delete(offerToRemove.getId());

        //then
        List<OfferModel> offerListAfterRemoval = offerRepository.findAll();
        assertThat(offerListAfterRemoval).hasSize(offerListBeforeUpdate.size() - 1);
    }

}