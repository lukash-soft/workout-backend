package com.lukash.workout.feature.invitation.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.invitation.dto.InvitationDto;
import com.lukash.workout.feature.invitation.model.Invitation;
import com.lukash.workout.feature.invitation.repository.InvitationRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import static com.lukash.workout.feature.invitation.model.InvitationStatus.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


class InvitationServiceTest extends IntegrationTest {
    @Autowired
    UserAccountRepository userAccountRepository;
    @Autowired
    InvitationRepository invitationRepository;
    @Autowired
    InvitationService invitationService;

    public Fixture fixture = new Fixture();

    //this method will be run after each test
    @AfterEach
    void cleanup() {
        invitationRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldGetPendingInvitations() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final UserAccount receiver = fixture.prepareAccount("receiver@gmail.com", AccountRole.CLIENT);
        userAccountRepository.save(receiver);

        Invitation sentInvitation = fixture.prepareInvitation(loggedUser, receiver);

        Invitation receivedInvitation = fixture.prepareInvitation(receiver, loggedUser);

        invitationRepository.save(sentInvitation);
        invitationRepository.save(receivedInvitation);
        //when
        InvitationDto result = invitationService.getPendingInvitations();
        // then
        assertThat(result.getSent()).hasSize(1);
        assertThat(result.getReceived()).hasSize(1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldDeclineInvitation() {
        //given
        final UserAccount receiver = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(receiver);
        final UserAccount sender = fixture.prepareAccount("sender@gmail.com", AccountRole.CLIENT);
        userAccountRepository.save(sender);

        Invitation invitationBeforeUpdate = fixture.prepareInvitation(sender, receiver);
        invitationRepository.save(invitationBeforeUpdate);
        //when
        invitationService.declineInvitation(invitationBeforeUpdate.getId());
        //then
        Invitation invitationToDeclineAfterUpdate = invitationRepository.findById(invitationBeforeUpdate.getId()).get();
        assertThat(invitationToDeclineAfterUpdate.getInvitationStatus()).isEqualTo(DECLINED);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldAcceptInvitation() {
        //given
        final UserAccount receiver = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        userAccountRepository.save(receiver);
        final UserAccount sender = fixture.prepareAccount("sender@gmail.com", AccountRole.CLIENT);
        userAccountRepository.save(sender);

        Invitation invitationBeforeUpdate = fixture.prepareInvitation(sender, receiver);
        invitationRepository.save(invitationBeforeUpdate);
        //when
        invitationService.acceptInvitation(invitationBeforeUpdate.getId());
        //then
        Invitation invitationToDeclineAfterUpdate = invitationRepository.findById(invitationBeforeUpdate.getId()).get();
        assertThat(invitationToDeclineAfterUpdate.getInvitationStatus()).isEqualTo(ACCEPTED);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldInviteUser() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final UserAccount invitedUser = fixture.prepareAccount("invited@gmail.com", AccountRole.CLIENT);
        userAccountRepository.save(invitedUser);

        //when
        invitationService.inviteUser(invitedUser.getId());
        //then
        Invitation invitation =
                invitationRepository.findAllByAccountToIdAndInvitationStatus(invitedUser.getId(), PENDING).get(0);
        assertThat(invitation.getAccountTo().getId()).isEqualTo(invitedUser.getId());
        assertThat(invitation.getInvitationStatus()).isEqualTo(PENDING);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void shouldThrowExceptionWhenInvitationExists() {
        //given
        final UserAccount sender = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(sender);
        final UserAccount receiver = fixture.prepareAccount("sender@gmail.com", AccountRole.CLIENT);
        userAccountRepository.save(receiver);

        Invitation invitation = fixture.prepareInvitation(sender, receiver);

        invitationRepository.save(invitation);

        //when, then
        assertThatThrownBy(() ->
                invitationService.inviteUser(receiver.getId())).isInstanceOf(RuntimeException.class);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void cancelInvitation() {
        //given
        final UserAccount sender = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(sender);
        final UserAccount receiver = fixture.prepareAccount("sender@gmail.com", AccountRole.CLIENT);
        userAccountRepository.save(receiver);

        final Invitation invitationToCancel = fixture.prepareInvitation(sender, receiver);
        invitationRepository.save(invitationToCancel);

        final int initialInvitationQuantity = invitationRepository.findAllByAccountFrom_Id(sender.getId()).size();

        //when
        invitationService.cancelInvitation(invitationToCancel.getId());
        //then
        final int afterCancelingInvitationQuantity = invitationRepository.findAllByAccountFrom_Id(sender.getId()).size();

        assertThat(afterCancelingInvitationQuantity).isEqualTo(initialInvitationQuantity - 1);
    }

    @Transactional
    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void friendToRemove() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final UserAccount friend = fixture.prepareAccount("sender@gmail.com", AccountRole.COACH);
        userAccountRepository.save(friend);

        loggedUser.getFriends().add(friend);
        friend.getFriends().add(loggedUser);
        //when
        invitationService.friendToRemove(friend.getId());
        //then
        assertThat(loggedUser.getFriends()).isEmpty();
        assertThat(friend.getFriends()).isEmpty();
    }
}
