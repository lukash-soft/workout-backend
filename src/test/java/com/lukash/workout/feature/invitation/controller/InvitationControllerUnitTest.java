package com.lukash.workout.feature.invitation.controller;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.invitation.model.Invitation;
import com.lukash.workout.feature.invitation.service.InvitationService;
import com.lukash.workout.setup.Fixture;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class InvitationControllerUnitTest {

    final Fixture fixture = new Fixture();

    @Mock
    InvitationService invitationService;
    @InjectMocks
    InvitationController invitationController;

    @Test
    void getPendingInvitations() {
        //given, when
        invitationController.getPendingInvitations();
        //then
        verify(invitationService, times(1)).getPendingInvitations();
    }

    @Test
    void inviteUser() {
        //given
        final UserAccount userToInvite = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);

        //when
        invitationController.inviteUser(userToInvite.getId());

        //then
        verify(invitationService, times(1)).inviteUser(userToInvite.getId());

    }

    @Test
    void cancelInvitation() {
        //given
        final Invitation invitation = createMockInvitation();

        //when
        invitationController.cancelInvitation(invitation.getId());

        //then
        verify(invitationService, times(1)).cancelInvitation(invitation.getId());
    }

    @Test
    void declineInvitation() {
        //given
        final Invitation invitation = createMockInvitation();

        //when
        invitationController.declineInvitation(invitation.getId());

        //then
        verify(invitationService, times(1)).declineInvitation(invitation.getId());

    }

    @Test
    void acceptInvitation() {
        //given
        final Invitation invitation = createMockInvitation();

        //when
        invitationController.acceptInvitation(invitation.getId());

        //then
        verify(invitationService, times(1)).acceptInvitation(invitation.getId());
    }

    @Test
    void removeFriend() {
        //given
        final UserAccount friendToRemove = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);

        //when
        invitationController.removeFriend(friendToRemove.getId());

        //then
        verify(invitationService, times(1)).friendToRemove(friendToRemove.getId());
    }

    private Invitation createMockInvitation() {
        return fixture.prepareInvitation(fixture.createCoach(), fixture.createClient());
    }
}