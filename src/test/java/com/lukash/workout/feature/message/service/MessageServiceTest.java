package com.lukash.workout.feature.message.service;

import com.lukash.workout.configuration.security.constants.AccountRole;
import com.lukash.workout.feature.account.model.UserAccount;
import com.lukash.workout.feature.account.repository.UserAccountRepository;
import com.lukash.workout.feature.message.model.Message;
import com.lukash.workout.feature.message.repository.MessageRepository;
import com.lukash.workout.setup.Fixture;
import com.lukash.workout.setup.IntegrationTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class MessageServiceTest extends IntegrationTest {

    @Autowired
    MessageService messageService;

    @Autowired
    MessageRepository messageRepository;

    @Autowired
    UserAccountRepository userAccountRepository;

    Fixture fixture = new Fixture();


    @AfterEach
    void cleanup() {
        messageRepository.deleteAll();
        userAccountRepository.deleteAll();
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void getAllForUser() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final UserAccount userTo = fixture.prepareAccount("userTo@gmil.com", AccountRole.CLIENT);
        userAccountRepository.save(userTo);
        int messageQuantity = 4;
        final List<Message> messages = fixture.prepareMessages(loggedUser, userTo, messageQuantity);
        messageRepository.saveAll(messages);

        final List<Message> messageList = messageService.getAllForUser(loggedUser.getId());
        //when, then
        assertThat(messageList).hasSize(messageQuantity);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void saveMessage() {
        //given

        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final UserAccount userTo = fixture.prepareAccount("userTo@gmil.com", AccountRole.CLIENT);
        userAccountRepository.save(userTo);

        final Message messageToSave = Message.builder()
                .created(LocalDateTime.now())
                .content("This is something new")
                .sender(loggedUser)
                .recipient(userTo)
                .build();
        //when
        messageService.saveMessage(messageToSave);
        //then
        final List<Message> messageList = messageRepository.findAll();
        assertThat(messageList).hasSize(1);
    }

    @Test
    @WithMockUser(Fixture.USER_EMAIL)
    void removeMessage() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final UserAccount userTo = fixture.prepareAccount("userTo@gmil.com", AccountRole.CLIENT);
        userAccountRepository.save(userTo);
        int initialMessageCount = 4;
        List<Message> messages = fixture.prepareMessages(loggedUser, userTo, initialMessageCount);
        messageRepository.saveAll(messages);

        final Message messageToRemove = messages.get(0);
        //when
        messageService.removeMessage(messageToRemove.getId());
        //then
        List<Message> messagesAfterRemoving = messageRepository.findAll();

        assertThat(messagesAfterRemoving).hasSize(messages.size() - 1);
    }

    @Test
    void shouldThrowExceptionWhenCreatedIsNull() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.COACH);
        userAccountRepository.save(loggedUser);
        final UserAccount userTo = fixture.prepareAccount("userTo@gmil.com", AccountRole.COACH);
        userAccountRepository.save(userTo);

        Message messageToTest = Message.builder()
                .created(null)
                .sender(loggedUser)
                .recipient(userTo)
                .content("This is something")
                .build();

        //when, then
        assertThatThrownBy(() -> messageService.saveMessage(messageToTest)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void shouldThrowExceptionWhenFromIsNull() {
        //given
        final UserAccount userTo = fixture.prepareAccount("userTo@gmil.com", AccountRole.CLIENT);
        userAccountRepository.save(userTo);

        Message messageToTest = Message.builder()
                .created(LocalDateTime.now())
                .sender(null)
                .recipient(userTo)
                .content("This is something new")
                .build();

        //when, then
        assertThatThrownBy(() -> messageService.saveMessage(messageToTest)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void shouldThrowExceptionWhenToIsNull() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        userAccountRepository.save(loggedUser);

        Message messageToTest = Message.builder()
                .created(LocalDateTime.now())
                .sender(loggedUser)
                .recipient(null)
                .content("This is something")
                .build();

        //when, then
        assertThatThrownBy(() -> messageService.saveMessage(messageToTest)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void shouldThrowExceptionWhenContentIsNull() {
        //given
        final UserAccount loggedUser = fixture.prepareAccount(Fixture.USER_EMAIL, AccountRole.CLIENT);
        userAccountRepository.save(loggedUser);
        final UserAccount userTo = fixture.prepareAccount("userTo@gmil.com", AccountRole.COACH);
        userAccountRepository.save(userTo);


        Message messageToTest = Message.builder()
                .created(LocalDateTime.now())
                .sender(loggedUser)
                .recipient(userTo)
                .content(null)
                .build();

        //when, then
        assertThatThrownBy(() -> messageService.saveMessage(messageToTest)).isInstanceOf(RuntimeException.class);
    }
}
